﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class JumpMovement : Movement
{
    public float jumpTime = 0.5f;
    public float jumpHeight = 1.1f;
    public List < AnimationStateInfo > tricks;

    private AnimationStateInfo currentTrick;
    private float currentTime = 0f;

    private LayerMask layer;

    public override AnimationStateInfo AnimationState
    {
        get { return currentTrick; }
    }

    public override LayerMask Layer
    {
        get { return layer; }
    }

    public override Lane Move()
    {
        Lane lane = Lane.kNull;

        if (character.swipeDirection == Direction.Right)
        {
            lane = character.currentLane.GetRight();
        }
        else if (character.swipeDirection == Direction.Left)
        {
            lane = character.currentLane.GetLeft();
        }

        return lane;
    }

    public override bool WantsControl()
    {
        bool control = false;

        if (character.currentMovement.Equals(this))
        {
            currentTime += Time.deltaTime;

            if (currentTime < jumpTime)
                control = true;

            SlideMovement slideMovement = (SlideMovement)character.movements.Find(x => x is SlideMovement);

            if (slideMovement.WantsControl())
                control = false;
        }
        else if (character.swipeDirection == Direction.Up)
        {
            if ((character.currentMovement.Layer & GameLayer.Upper) != 0)
                layer = GameLayer.Upper;
            else
                layer = GameLayer.Above;

            character.transform.DOScale(jumpHeight, jumpTime * 0.5f)
                .OnComplete(() => character.transform.DOScale(1f, jumpTime * 0.5f));

            control = true;

            currentTrick = tricks[UnityEngine.Random.Range(0, tricks.Count)];
        }
        if (control == false)
        {
            Transform hit = character.collision2D.hazardCollisions.below.transform;

            if (hit)
            {
                JumpTile jumpTile = hit.GetComponent < JumpTile >();

                if (jumpTile)
                {
                    control = true;
                    currentTrick = AnimationStateInfo.GrabFS;
                    layer = GameLayer.Upper;
                }
                   
            }
        }

        if (control == true && character.currentMovement is SlideMovement)
        {
            character.overrideAnimation = currentTrick;
        }

        return control;
    }

    public override void GainingControl()
    {
        currentTime = 0f;
    }

    public override void LosingControl()
    {
        currentTime = 0f;
    }
}
