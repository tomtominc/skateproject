﻿using UnityEngine;

public class LaneMovement : Movement
{
    public override AnimationStateInfo AnimationState
    { 
        get
        {
            AnimationStateInfo animation = AnimationStateInfo.Normal;

            if (character.isMoving)
            {
                float direction = Mathf.Sign(character.transform.position.x - character.currentLane.position.x);

                animation = direction == 1 ? AnimationStateInfo.MoveRight : AnimationStateInfo.MoveLeft;
            }

            return animation;
        }
    }

    public override LayerMask Layer
    {
        get { return GameLayer.Middle; }
    }

    public override bool WantsControl()
    {
        return true;
    }

    public override Lane Move()
    {
        Lane lane = character.currentLane;

        if (character.swipeDirection == Direction.Right)
        {
            lane = character.currentLane.GetRight();
        }
        else if (character.swipeDirection == Direction.Left)
        {
            lane = character.currentLane.GetLeft();
        }

        return lane;
    }

    public override void GainingControl()
    {
    }

    public override void LosingControl()
    {
    }

    
}
