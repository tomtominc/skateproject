﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CutSceneMovement : Movement
{
    public float cutSceneTimeScale = 0.8f;
    public float durationToTimeScale = 0.5f;
    private CutSceneTile _tile;

    public override AnimationStateInfo AnimationState
    {
        get { return _tile.animationOverride; }
    }

    public override LayerMask Layer
    {
        get{ return GameLayer.Above; }
    }

    public override Lane Move()
    {
        return Lane.kNull;
    }

    public override bool WantsControl()
    {
        Transform hit = character.collision2D.hazardCollisions.below.transform;

        if (hit)
        {
            _tile = hit.GetComponent<CutSceneTile>();

            if (_tile == null)
                return false;

            if ((_tile.activationLayers & character.currentMovement.Layer) == 0)
                return false;


            return true;
        }

        return false;
    }

    public override void GainingControl()
    {
        //DOTween.To(() => Time.timeScale, x => Time.timeScale = x, cutSceneTimeScale, durationToTimeScale);
    }

    public override void LosingControl()
    {
        //Time.timeScale = 1f;
        //DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1f, durationToTimeScale);
    }
}
