﻿using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class DeathMovement : Movement
{
    public float shakeForce = 1.0f;
    private Lane deathLane;
    private AnimationStateInfo deathAnimation;

    public override AnimationStateInfo AnimationState
    {
        get { return deathAnimation; }
    }

    public override LayerMask Layer
    {
        get{ return GameLayer.Below; }
    }

    public override Lane Move()
    {
        return deathLane;
    }

    public override bool WantsControl()
    {
        bool wantsControl = false;

        Transform front = character.collision2D.hazardCollisions.front.transform;
        Transform right = character.collision2D.hazardCollisions.right.transform;
        Transform left = character.collision2D.hazardCollisions.left.transform;

        if (front && front.GetComponent < Hazard >())
        {
            Hazard hazard = front.transform.GetComponent<Hazard>();

            if ((hazard.BlockingLayers & character.currentMovement.Layer) != 0)
            {
                hazard.Flash();
                deathLane = Lane.kNull;
                wantsControl = true;
                deathAnimation = hazard.deathAnimation;
                GameManager.Instance.ShakeCamera(new Vector3(0, 1f, 0));
            }
        }

        if (right && right.GetComponent < Hazard >())
        {
            Hazard hazard = right.transform.GetComponent<Hazard>();

            if ((hazard.BlockingLayers & character.currentMovement.Layer) != 0)
            {
                deathLane = character.currentLane.GetLeft();
                wantsControl = true;
                deathAnimation = hazard.deathAnimation;
                GameManager.Instance.ShakeCamera(new Vector3(1f, 0f, 0f));
            }
        }

        if (left && left.GetComponent < Hazard >())
        {
            Hazard hazard = left.transform.GetComponent<Hazard>();

            if ((hazard.BlockingLayers & character.currentMovement.Layer) != 0)
            {
                deathLane = character.currentLane.GetRight();
                wantsControl = true;
                deathAnimation = hazard.deathAnimation;
                GameManager.Instance.ShakeCamera(new Vector3(1f, 0f, 0f));
            }
        }

        if (character.IsPowerActive(PowerType.Shield) && wantsControl)
        {
            GameManager.Instance.RefreshGame();
            wantsControl = false;
        }

        return wantsControl;
    }

    public override void GainingControl()
    {
    }

    public override void LosingControl()
    {
    }
}
