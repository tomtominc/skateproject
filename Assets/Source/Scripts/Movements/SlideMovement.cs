﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SlideMovement : Movement
{
    public float slideTime = 0.5f;
    private float currentTime = 0f;

    public override AnimationStateInfo AnimationState
    {
        get { return AnimationStateInfo.Slide; }
    }

    public override LayerMask Layer
    {
        get { return GameLayer.Below; }
    }

    public override Lane Move()
    {
        Lane lane = Lane.kNull;

        if (character.swipeDirection == Direction.Right)
        {
            lane = character.currentLane.GetRight();
        }
        else if (character.swipeDirection == Direction.Left)
        {
            lane = character.currentLane.GetLeft();
        }

        return lane;
    }

    public override bool WantsControl()
    {
        bool control = false;

        if (character.currentMovement.Equals(this))
        {
            currentTime += Time.deltaTime;

            if (currentTime < slideTime)
                control = true;
        }
        else if (character.swipeDirection == Direction.Down)
        {
            control = true;
        }

        return control;
    }

    public override void GainingControl()
    {
        currentTime = 0f;
    }

    public override void LosingControl()
    {
        currentTime = 0f;
    }
}
