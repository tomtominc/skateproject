﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;
using System.Text;

public enum AnimationStateInfo
{
    None,
    Normal,
    MoveRight,
    MoveLeft,
    ShoveItFS,
    ShoveItBS,
    Kickflip,
    Heelflip,
    GrabFS,
    FiftyFifty,
    BoardSlide,
    Slide,
    Death,
    Electrocute,
    Fall,
    Recover
}

public struct Lane
{
    public enum Id
    {
        Null,
        Left,
        Middle,
        Right
    }

    public Id id;
    public Vector3 position;

    public Lane(Id id, Vector3 position)
    {
        this.id = id;
        this.position = position;
    }

    public Lane GetRight()
    {
        if (id.Equals(kNull))
            return kNull;
        
        return id.Equals(Left.id) ? Middle : Right; 
    }

    public Lane GetLeft()
    {
        if (id.Equals(kNull))
            return kNull;

        return id.Equals(Right.id) ? Middle : Left; 
    }

    public static float YPos = 2.5f;
    public static Lane kNull = new Lane(Lane.Id.Null, Vector3.zero);
    public static Lane Left = new Lane(Lane.Id.Left, new Vector3(-1f, YPos));
    public static Lane Middle = new Lane(Lane.Id.Middle, new Vector3(0f, YPos));
    public static Lane Right = new Lane(Lane.Id.Right, new Vector3(1f, YPos));
}

public abstract class Movement : MonoBehaviour
{
    internal Entity _entity;

    public Character character
    {
        get { return (Character)_entity; }
    }

    private void Start()
    {
        _entity = GetComponentInParent<Entity>();
    }

    public virtual AnimationStateInfo AnimationState
    {
        get { return AnimationStateInfo.None; }
    }

    public virtual LayerMask Layer
    {
        get { return GameLayer.Middle; }
    }

    public Vector3 position
    { 
        get { return transform.position; } 
    }

    public abstract Lane Move();

    public abstract bool WantsControl();

    public abstract void GainingControl();

    public abstract void LosingControl();
    
}
