﻿using UnityEngine;

public class PlatformMovement : Movement
{
    protected IPlatform platform;
    protected AnimationStateInfo platformAnimation;

    public override AnimationStateInfo AnimationState
    {
        get
        {
            AnimationStateInfo animation = platformAnimation;

            if (character.isMoving)
            {
                float direction = Mathf.Sign(character.transform.position.x - character.currentLane.position.x);

                animation = direction == 1 ? AnimationStateInfo.MoveRight : AnimationStateInfo.MoveLeft;
            }

            return animation;
        }
    }

    public override LayerMask Layer
    {      
        get
        {
            return platform.Layers.GetMaxSetValue();
        }      
    }

    public override bool WantsControl()
    {
        Transform hit = character.collision2D.hazardCollisions.below.transform;

        if (hit)
        {
            platform = hit.GetComponent<IPlatform>();

            if (platform == null)
                return false;

            if ((platform.Layers & character.currentMovement.Layer) == 0)
                return false;

            if (!character.currentMovement.Equals(this))
            {
                if (character.currentAnimation == AnimationStateInfo.Heelflip || character.currentAnimation == AnimationStateInfo.Kickflip)
                    platformAnimation = AnimationStateInfo.FiftyFifty;
                else
                    platformAnimation = AnimationStateInfo.BoardSlide;
            }

            return true;
        }

        return false;
    }

    public override Lane Move()
    {
        Lane lane = character.currentLane;

        if (character.swipeDirection == Direction.Zero && character.isMoving == false)
        {
            lane = platform.ToLane;
        }
        else if (character.swipeDirection == Direction.Right)
        {
            lane = character.currentLane.GetRight();
        }
        else if (character.swipeDirection == Direction.Left)
        {
            lane = character.currentLane.GetLeft();
        }

        return lane;

    }

    public override void GainingControl()
    {
    }

    public override void LosingControl()
    {
        character.overrideAnimation = AnimationStateInfo.Recover;
    }
}
