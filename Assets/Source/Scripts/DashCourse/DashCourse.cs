﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashCourse : Singleton<DashCourse>
{
    public float zoneDuration = 20f;

    public List < Zone > AZones;
    public List < Zone > BZones;
    public List < Zone > BonusZones;

    private float currentZoneDuration = 0f;
    private Queue < Zone > courseQueue;
    private Zone currentZone;

    public void CreateZoneSequence()
    {
        courseQueue = new Queue < Zone >();

        courseQueue.Enqueue(AZones[Random.Range(0, AZones.Count)]);    
        courseQueue.Enqueue(AZones[Random.Range(0, AZones.Count)]);    
        courseQueue.Enqueue(BZones[Random.Range(0, BZones.Count)]);    
        courseQueue.Enqueue(AZones[Random.Range(0, AZones.Count)]);    
        courseQueue.Enqueue(AZones[Random.Range(0, AZones.Count)]);
        courseQueue.Enqueue(BZones[Random.Range(0, BZones.Count)]);
    }

    public void StartZoneSequence()
    {
        StartCoroutine("UpdateZoneSequence");
    }

    public void EndZoneSequence()
    {
        StopCoroutine("UpdateZoneSequence");
    }

    public IEnumerator UpdateZoneSequence()
    {
        while (courseQueue.Count > 0)
        {
            currentZone = courseQueue.Dequeue();

            World.Instance.ChangeZone(currentZone);

            if (World.Instance.HasCompleteCondition())
            {
                while (!World.Instance.ZoneComplete())
                {
                    World.Instance.UpdateGameWorld();
                    yield return null;
                }
            }
            else
            {
                currentZoneDuration += World.Instance.score + zoneDuration;

                while (World.Instance.score < currentZoneDuration)
                {
                    World.Instance.UpdateGameWorld();
                    yield return null;
                }
                    
            }
        }
    }
}
