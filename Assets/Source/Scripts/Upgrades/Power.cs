﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

public enum PowerType
{
    None = 0,
    Magnet = 1 << 1,
    CoinDoubler = 1 << 2,
    Shield = 1 << 3,
    RocketBoard = 1 << 4
}

[System.Serializable]
public class Power
{
    public PowerType powerType;
    public float minDuration = 5f;
    public SpriteAnimation powerEffect;

    private int level;
    private float duration;
    private Character character;

    private Sequence disableSequence;

    public void Initialize(Character character)
    {
        this.character = character;
    }

    public void StartPower()
    {
        level = PlayerPrefs.GetInt(powerType.ToString(), 0);
        duration = minDuration + level;

        if (disableSequence != null)
            disableSequence.Kill();

        disableSequence = DOTween.Sequence();
        disableSequence.AppendInterval(duration);
        disableSequence.AppendCallback(() => character.DeactivatePower(powerType));

        powerEffect.Play(powerType);
    }

    public void EndPower()
    {
        powerEffect.Play(PowerType.None);
    }
}
