﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITile
{
}

[RequireComponent(typeof(BoxCollider2D))]
public class CutSceneTile : MonoBehaviour, ITile
{
    public LayerMask activationLayers;
    public LayerMask characterLayerOverride;
    public AnimationStateInfo animationOverride;
}
