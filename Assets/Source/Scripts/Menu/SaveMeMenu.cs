﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using DoozyUI;

public class SaveMeMenu : Singleton<SaveMeMenu>
{
    public float saveMeTime = 4f;
    public Slider saveMeCounter;
    public Button saveMeButton;

    private Tweener saveMeTween;

    public void StartSaveCounter(Action onComplete)
    {
        if (saveMeTween != null)
            saveMeTween.Kill();

        saveMeButton.interactable = true;
        saveMeCounter.value = 1f;

        saveMeTween = saveMeCounter.DOValue(0f, saveMeTime).SetEase(Ease.Linear).SetDelay(0.5f).OnComplete 
        (() =>
            {
                saveMeButton.interactable = false;
                
                if (onComplete != null)
                    onComplete();
            });
    }

    public void DoSaveMeVideo()
    {
        if (saveMeTween != null)
            saveMeTween.Kill();

        UIManager.HideUiElement("SaveMeMenu");

        AdsManager.Instance.ShowRewardedAd(AdsManager.AdRewardType.SaveMe, (data) =>
            {
                if (data.finished)
                {
                    
                    GameManager.Instance.RefreshGame();
                }
            });
    }

    public void Restart_ButtonEvent()
    {
        if (saveMeTween != null)
            saveMeTween.Kill();

        UIManager.HideUiElement("SaveMeMenu");

        GameManager.Instance.ChangeState(GameState.GameOver);
    }
}
