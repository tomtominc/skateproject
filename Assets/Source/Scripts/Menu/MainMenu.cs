﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text inGameCurrencyLabel;
    public Text upgradeCurrencyLabel;
    public Text premiumCurrencyLabel;

    public Text bestLabel;

    public void OnEnable()
    {
        if (Bag.Instance == null)
            return;
        
        inGameCurrencyLabel.text = Bag.Get(ItemKey.Coins).Value.ToString("D2");
        upgradeCurrencyLabel.text = Bag.Get(ItemKey.Scraps).Value.ToString("D2");
        premiumCurrencyLabel.text = Bag.Get(ItemKey.Gems).Value.ToString("D2");
        bestLabel.text = Bag.Get(ItemKey.Meters).BestValue.ToString("D2");
    }
}
