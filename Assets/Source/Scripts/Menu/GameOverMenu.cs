﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameOverMenu : Singleton < GameOverMenu >
{
    public Text metersLabel;
    public Text goldCollectedLabel;
    public Button doubleItButton;

    public AudioKey tallyUpSound;

    private float duration = 1f;

    public void Init()
    {
        int meters = 0;
        int gold = 0;

        int maxMeters = Bag.Get(ItemKey.Meters).SessionValue;
        int maxCoins = Bag.Get(ItemKey.Coins).SessionValue;

        metersLabel.text = "00";
        goldCollectedLabel.text = "+00";

        doubleItButton.interactable = true;

        Sequence sequence = DOTween.Sequence();
        sequence.AppendInterval(0.5f);

        sequence.Append(DOTween.To(() => meters, x => meters = x, maxMeters, duration)
            .SetEase(Ease.Linear)/*.OnStart(() => AudioStorage.Instance.Play(tallyUpSound, 3))*/.OnUpdate(() =>
                {
                    metersLabel.text = meters.ToString("D2");
                })/*.OnComplete(() => AudioStorage.Instance.StopMusic(3))*/);

        sequence.Append(DOTween.To(() => gold, x => gold = x, maxCoins, duration)
            .SetEase(Ease.Linear)/*.OnStart(() => AudioStorage.Instance.Play(tallyUpSound, 4))*/.OnUpdate(() =>
                {
                    goldCollectedLabel.text = string.Format("+{0}", gold.ToString("D2"));
                })/*.OnComplete(() => AudioStorage.Instance.StopMusic(4))*/);

    }

    public void Restart_ButtonEvent()
    {
        GameManager.Instance.ChangeState(GameState.PlayGame);
    }

    public void BackToMenu_ButtonEvent()
    {
        GameManager.Instance.ChangeState(GameState.Main);
    }

    public void DoubleItWithAd_ButtonEvent()
    {
        doubleItButton.interactable = false;

        AdsManager.Instance.ShowRewardedAd(AdsManager.AdRewardType.DoubledIt, (data) =>
            {
                if (data.finished)
                {
                    int coins = Bag.Get(ItemKey.Coins).SessionValue;

                    Bag.Add(ItemKey.Coins, coins);

                    int maxCoins = coins * 2;

                    DOTween.To(() => coins, x => coins = x, maxCoins, duration).SetEase(Ease.Linear).OnUpdate(() =>
                        {
                            goldCollectedLabel.text = string.Format("+{0}", coins.ToString("D2"));
                        });
                }
            });
    }

    public void DoubleItWithPremiumCurrency_ButtonEvent()
    {

    }
}
