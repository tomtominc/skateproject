﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class HazardSpriteLayer : MonoBehaviour
{
    public string layerBeforeCharacterPasses;
    public string layerAfterCharacterPasses;

    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent < SpriteRenderer >();
    }

    private void Update()
    {
        if (Character.Instance.position.y > transform.position.y)
            spriteRenderer.sortingLayerName = layerBeforeCharacterPasses;
        else
            spriteRenderer.sortingLayerName = layerAfterCharacterPasses;
    }
}
