﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugCanvas : MonoBehaviour
{
    public Text debugLabel;
    public Text FPSLabel;
    public Character character;

    float dt = 0.0f;
    float fps = 0.0f;

    private void Update()
    {
        dt += (Time.deltaTime - dt) * 0.1f;
        fps = dt * 1000.0f;

        if (character && character.currentMovement != null)
        {
            debugLabel.text = string.Format
                ("State:<color=white>{0}</color>\n" +
                "Animation:<color=white>{1}</color>\n" +
                "Layer:<color=white>{2}</color>\n" +
                "Speed:<color=white>{3}</color>\n" +
                "Coins:<color=white>{4}</color>\n" +
                "FPS:<color=white>{5:0.0}</color>",
                character.currentMovement, 
                character.currentMovement.Layer.MaskToString(),
                character.currentMovement.AnimationState,
                World.Instance.currentSpeed,
                Bag.Get(ItemKey.Coins).SessionValue,
                fps);
        }
        else
        {
            debugLabel.text = string.Format("FPS:<color=white>{0}</color>", fps);
        }
//
    
    }

}
