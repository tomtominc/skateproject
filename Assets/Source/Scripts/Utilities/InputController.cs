﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private float fingerStartTime = 0.0f;
    private Vector2 fingerStartPos = Vector2.zero;

    private bool isSwipe = false;
    private float minSwipeDist = 50.0f;
    private float maxSwipeTime = 0.5f;

    public Direction UpdateSwipe()
    {
        #if UNITY_EDITOR
        return GetSwipeDirectionPC();
        #else
        return GetSwipeDirectionMobile();
        #endif
    }

    private Direction GetSwipeDirectionPC()
    {
        Direction dir = Direction.Zero;

        if (Input.GetKeyDown(KeyCode.W))
            dir = Direction.Up;
        if (Input.GetKeyDown(KeyCode.S))
            dir = Direction.Down;
        if (Input.GetKeyDown(KeyCode.A))
            dir = Direction.Left;
        if (Input.GetKeyDown(KeyCode.D))
            dir = Direction.Right;

        return dir;
    }

    private Direction GetSwipeDirectionMobile()
    {
        if (Input.touchCount <= 0)
            return Direction.Zero;

        Direction dir = Direction.Zero;
        
        foreach (Touch touch in Input.touches)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    isSwipe = true;
                    fingerStartTime = Time.time;
                    fingerStartPos = touch.position;
                    break;

                case TouchPhase.Canceled:
                    isSwipe = false;
                    break;

                case TouchPhase.Ended:

                    float gestureTime = Time.time - fingerStartTime;
                    float gestureDist = (touch.position - fingerStartPos).magnitude;

                    if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minSwipeDist)
                    {
                        Vector2 direction = touch.position - fingerStartPos;
                        Vector2 swipeType = Vector2.zero;

                        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                        {
                            // the swipe is horizontal:
                            swipeType = Vector2.right * Mathf.Sign(direction.x);
                        }
                        else
                        {
                            // the swipe is vertical:
                            swipeType = Vector2.up * Mathf.Sign(direction.y);
                        }

                        if (swipeType.x != 0.0f)
                        {
                            if (swipeType.x > 0.0f)
                            {
                                dir = Direction.Right;
                            }
                            else
                            {
                                dir = Direction.Left;
                            }
                        }

                        if (swipeType.y != 0.0f)
                        {
                            if (swipeType.y > 0.0f)
                            {
                                dir = Direction.Up;
                            }
                            else
                            {
                                dir = Direction.Down;
                            }
                        }

                    }

                    break;
            }
        }

        return dir;

    }
}
