﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Principal;

public class SortingControl : MonoBehaviour
{
    public Vector2 pivot;
    private SpriteRenderer spriteRenderer;
    private SpriteRenderer shadowRenderer;

    private void Start()
    {
        spriteRenderer = GetComponentInChildren < SpriteRenderer >();
        Transform shadow = spriteRenderer.transform.Find("Shadow");

        if (shadow != null)
            shadowRenderer = shadow.GetComponent < SpriteRenderer >();
    }
	
    // Update is called once per frame
    void Update()
    {
        Vector2 anchor = pivot + (Vector2)transform.position;
        int sortOrder = -(int)(anchor.y * 100f);

        spriteRenderer.sortingOrder = sortOrder;

        if (shadowRenderer)
            shadowRenderer.sortingOrder = sortOrder - 1;
    }

}
