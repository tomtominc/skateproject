﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<T>();

            return instance;
        }
    }

    private void Awake ()
    {
        if (Instance != this)
            Destroy(gameObject);
    }

    protected static U Component < U > ()
    {
        return Instance.GetComponent<U>();
    }
}
