﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFXSort : MonoBehaviour
{
    private Character character;
    private SpriteRenderer spriteRenderer;

    private void Start()
    {
        character = Character.Instance;
        spriteRenderer = GetComponent < SpriteRenderer >();
    }

    private void LateUpdate()
    {
        spriteRenderer.sortingLayerName = character.spriteRenderer.sortingLayerName;
        spriteRenderer.sortingOrder = character.spriteRenderer.sortingOrder - 1;
    }
}
