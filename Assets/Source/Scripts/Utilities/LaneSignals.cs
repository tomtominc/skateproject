﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using DG.Tweening;

public class LaneSignals : MonoBehaviour
{
    public bool debug = false;
    public int numberOfBlinks = 3;
    public float blinkWaitTime = 0.1f;

    public SpriteRenderer leftLane;
    public SpriteRenderer middleLane;
    public SpriteRenderer rightLane;

    public Animator leftLaneEffect;
    public Animator middleLaneEffect;
    public Animator rightLaneEffect;

    private IEnumerator Start()
    {
        if (debug)
        {
            float timeBetween = 2f;
            yield return new WaitForSeconds(timeBetween);
            TriggerElectricEffect(true, false, true);
            yield return new WaitForSeconds(timeBetween);
            TriggerElectricEffect(false, true, true);
            yield return new WaitForSeconds(timeBetween);
            TriggerElectricEffect(true, true, false);
            yield return new WaitForSeconds(timeBetween);
            TriggerElectricEffect(false, false, true);
        }
    }

    public void TriggerElectricEffect(bool left, bool middle, bool right)
    {
        if (left)
            ElectricEffect(leftLane, leftLaneEffect);
        if (middle)
            ElectricEffect(middleLane, middleLaneEffect);
        if (right)
            ElectricEffect(rightLane, rightLaneEffect);
    }

    public void ElectricEffect(SpriteRenderer s, Animator a)
    {
        Sequence sequence = DOTween.Sequence();

        for (int i = 0; i < numberOfBlinks; i++)
        {
            sequence.AppendCallback(() => s.color = new Color(s.color.r, s.color.g, s.color.b, 0.5f));
            sequence.AppendInterval(blinkWaitTime);
            sequence.AppendCallback(() => s.color = new Color(s.color.r, s.color.g, s.color.b, 0f));
            sequence.AppendInterval(blinkWaitTime);
        }

        sequence.AppendCallback(() => a.Play("Activate"));
    }
}
