﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

[RequireComponent(typeof(BoxCollider2D))]
public class Collision2D : MonoBehaviour
{
    public LayerMask hazardLayers;
    public LayerMask itemLayers;
    public LayerMask platformLayers;
    public int horizontalRayCount = 4;
    public int verticalRayCount = 4;
    public int forwardRayCount = 4;

    public CollisionInfo hazardCollisions;
    public CollisionInfo itemCollisions;

    private float horizontalRaySpacing;
    private float verticalRaySpacing;
    private float belowRaySpacing;

    private BoxCollider2D _collider;
    private RaycastOrigins raycastOrigins;
    private Character character;


    private void Start()
    {
        _collider = GetComponent < BoxCollider2D >();
        character = GetComponent<Character>();
        CalculateRaySpacing();
    }

    private void UpdateRaycastOrigins()
    {
        Bounds bounds = _collider.bounds;
        bounds.Expand(World.pixelSize * -2f);

        raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
        raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
        raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
        raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
       
    }

    private void CalculateRaySpacing()
    {
        Bounds bounds = _collider.bounds;
        bounds.Expand(World.pixelSize * -2f);

        horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
        verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        belowRaySpacing = bounds.size.y / (forwardRayCount - 1);
    }

    public void UpdateCollisions()
    {
        hazardCollisions.Reset();
        itemCollisions.Reset();

        UpdateRaycastOrigins();

        if (character.isMoving)
            HorizontalCollisions();

        if (!character.isMoving)
            VerticalCollisions();

        BelowCollisions();
    }

    public void HorizontalCollisions()
    {
        float itemRayLength = World.pixelSize * 2f;
        float hazardRayLength = World.pixelSize * 2f;
        float direction = Mathf.Sign(character.currentLane.position.x - character.transform.position.x);

        for (int i = 0; i < horizontalRayCount; i++)
        {
            Vector2 rayOrigin = direction == -1 ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);
            RaycastHit2D hazardHit = Physics2D.Raycast(rayOrigin, Vector2.right * direction, hazardRayLength, hazardLayers);
            RaycastHit2D itemHit = Physics2D.Raycast(rayOrigin, Vector2.right * direction, itemRayLength, itemLayers);

            Debug.DrawRay(rayOrigin, Vector2.right * hazardRayLength, Color.red);

            if (hazardHit.transform)
            {
                hazardRayLength = hazardHit.distance;
                hazardCollisions.right = direction == 1 ? hazardHit : new RaycastHit2D();
                hazardCollisions.left = direction == -1 ? hazardHit : new RaycastHit2D();
            }

            if (itemHit.transform)
            {
                itemRayLength = itemHit.distance;
                itemCollisions.right = direction == 1 ? itemHit : new RaycastHit2D();
                itemCollisions.left = direction == -1 ? itemHit : new RaycastHit2D();
            }

        }
    }

    public void VerticalCollisions()
    {
        float itemRayLength = World.pixelSize * 2f;
        float hazardRayLength = World.pixelSize * 2f;

        for (int i = 0; i < verticalRayCount; i++)
        {
            Vector2 rayOrigin = raycastOrigins.bottomLeft;
            rayOrigin += Vector2.right * (verticalRaySpacing * i);
            RaycastHit2D hazardHit = Physics2D.Raycast(rayOrigin, Vector2.down, hazardRayLength, hazardLayers);
            RaycastHit2D itemHit = Physics2D.Raycast(rayOrigin, Vector2.down, itemRayLength, itemLayers);

            Debug.DrawRay(rayOrigin, Vector2.down * hazardRayLength, Color.red);

            if (hazardHit.transform)
            {
                hazardRayLength = hazardHit.distance;
                hazardCollisions.front = hazardHit;
            }

            if (itemHit.transform)
            {
                itemRayLength = itemHit.distance;
                itemCollisions.front = itemHit;
            }

        }
    }

    public void BelowCollisions()
    {
        float rayLength = World.pixelSize * 2f;

        for (int i = 0; i < forwardRayCount; i++)
        {
            Vector2 rayOrigin = raycastOrigins.bottomLeft + new Vector2(0.5f, 0f);
            rayOrigin += Vector2.up * (belowRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.zero, rayLength, platformLayers);

            Debug.DrawRay(rayOrigin, Vector3.forward * 100f, Color.blue);

            if (hit.transform)
            {
                hazardCollisions.below = hit;
            }

        }
    }

    struct RaycastOrigins
    {
        public Vector2 topLeft, topRight;
        public Vector2 bottomLeft, bottomRight;
    }

    public struct CollisionInfo
    {
        public RaycastHit2D front, below;
        public RaycastHit2D left, right;

        public void Reset()
        {
            front = below = new RaycastHit2D();
            left = right = new RaycastHit2D();
        }
    }
}
