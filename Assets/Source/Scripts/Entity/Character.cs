﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using System.Linq;

public enum CharacterType
{
    Any,
    Critter_0,
    Critter_1,
    Critter_2
}

public class Character : Entity
{
    private static Character instance;

    public static Character Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < Character >();

            return instance;
        }
    }

    public float speed = 4f;
    public Transform container;
    public List < Power > powers;

    internal List<Movement> movements;

    internal Direction swipeDirection;
    internal InputController input;
    internal SpriteRenderer spriteRenderer;

    internal Collision2D collision2D;
    internal Lane currentLane;

    private SpriteAnimation _animator;
    internal Movement currentMovement;

    internal bool update = true;
    internal bool isPlayingAnimation = false;
    internal AnimationStateInfo currentAnimation;

    internal AnimationStateInfo overrideAnimation = AnimationStateInfo.None;

    internal PowerType activePowers;

    internal bool isMoving
    {
        get { return !transform.position.Equals(currentLane.position); }
    }

    internal bool isDead
    {
        get
        {
            return currentMovement is DeathMovement;
        }
    }

    private void Start()
    {
        _animator = GetComponentInChildren < SpriteAnimation >();

        spriteRenderer = GetComponentInChildren < SpriteRenderer >();
        input = GetComponent<InputController>();
        collision2D = GetComponent < Collision2D >();

        movements = GetComponentsInChildren<Movement>().ToList();
        powers.ForEach(power => power.Initialize(this));

        currentMovement = movements.Find(x => x is LaneMovement);
        currentLane = Lane.Middle;
    }

    public void AddToContainer(Transform obj, Vector3 offset = default(Vector3))
    {
        obj.SetParent(container);
        obj.localPosition = offset;
    }

    public void Initialize(GameState state)
    {
        if (state == GameState.PlayGame)
        {
            currentMovement = movements.Find(x => x is LaneMovement);

        }
        else if (state == GameState.Main)
        {
            transform.position = new Vector3(0f, 1f, 0f);

        }
        else if (state == GameState.PlayGameAfterSave)
        {
            transform.position = Lane.Middle.position;
            currentMovement = movements.Find(x => x is LaneMovement);
        }
        currentLane = Lane.Middle;
        swipeDirection = Direction.Zero;
        container.DestroyChildren();

        _animator.Play(AnimationStateInfo.Normal);
    }

    public void UpdateControls()
    {
        swipeDirection = input.UpdateSwipe();
        collision2D.UpdateCollisions();

        CheckForTriggers();
        CalculateMovement();
        MoveCharacter();


        CalculateSpriteDrawOrder();
        UpdateAnimation();
        CheckForDeath();
    }

    public void CheckForTriggers()
    {
        Transform frontHit = collision2D.itemCollisions.front.transform;
        Transform rightHit = collision2D.itemCollisions.right.transform;
        Transform leftHit = collision2D.itemCollisions.left.transform;

        if (frontHit)
        {
            TryTrigger(frontHit);
        }

        if (rightHit)
        {
            TryTrigger(rightHit);
        }

        if (leftHit)
        {
            TryTrigger(leftHit);
        }
    }


    public void TryTrigger(Transform trigger)
    {
        if (trigger.GetComponent < Trigger >())
        {
            trigger.GetComponent < Trigger >().DoTrigger();
        }

        Pickup pickUp = trigger.GetComponent < Pickup>();

        if (pickUp != null && (pickUp.collisionLayers & currentMovement.Layer) != 0)
        {
            pickUp.Collect();
        }
    }


    public void CalculateMovement()
    {
        Movement newMovement = null;

        foreach (var movement in movements)
        {
            if (movement.WantsControl())
            {
                newMovement = movement;
                break;
            }
        }

        if (newMovement != null && !newMovement.Equals(currentMovement))
        {
            currentMovement.LosingControl();
            currentMovement = newMovement;
            currentMovement.GainingControl();
        }
    }

    public void MoveCharacter()
    {
        Lane targetLane = currentMovement.Move();

        if (!targetLane.Equals(Lane.kNull))
            currentLane = targetLane;

        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentLane.position, speed * Time.deltaTime);
        }
    }

    private void CalculateSpriteDrawOrder()
    {
        spriteRenderer.sortingLayerName = currentMovement.Layer.MaskToString();
    }



    private void CheckForDeath()
    {
        if (isDead)
        {
            DeactivatePower(PowerType.Magnet);
            DeactivatePower(PowerType.CoinDoubler);
            DeactivatePower(PowerType.RocketBoard);
            DeactivatePower(PowerType.Shield);

            GameManager.Instance.GameOver();
        }
    }

    public float GetAnimationLength()
    {
        Debug.LogFormat("Anim: {0} Length: {1}", _animator.currentAnimationName, _animator.GetCurrentAnimationLength());
        return _animator.GetCurrentAnimationLength();
    }

    public void UpdateAnimation()
    {
        if (overrideAnimation != AnimationStateInfo.None &&
            !_animator.currentAnimationName.Equals(overrideAnimation.ToString()))
        {
            isPlayingAnimation = true;
            _animator.PlayWithCallBack(overrideAnimation, (x, y) =>
                {
                    overrideAnimation = AnimationStateInfo.None;
                    isPlayingAnimation = false;
                });

            return;
        }

        if (!_animator.currentAnimationName.Equals
            (currentMovement.AnimationState.ToString()) &&
            (!isPlayingAnimation || (int)currentMovement.AnimationState > (int)currentAnimation))
        {
            isPlayingAnimation = true;
            currentAnimation = currentMovement.AnimationState;
            _animator.PlayWithCallBack(currentMovement.AnimationState, (x, y) =>
                {
                    isPlayingAnimation = false;
                });
        }
    }

    public void ActivatePower(PowerType powerType)
    {
        Power power = powers.Find(x => x.powerType == powerType);

        if (power != null)
        {
            power.StartPower();
            activePowers = activePowers | power.powerType;
        }
    }

    public void DeactivatePower(PowerType powerType)
    {
        if (!IsPowerActive(powerType))
            return;

        Power power = powers.Find(x => x.powerType == powerType);

        if (power != null)
        {
            power.EndPower();
            activePowers = activePowers ^ power.powerType;
        }
    }

    public bool IsPowerActive(PowerType powerType)
    {
        return (activePowers & powerType) == powerType;
    }
}
