﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
public class GameOverState : BaseGameState
{
    private const string kGameOverMenu = "GameOver";

    public override void StateStart()
    {
        UIManager.ShowUiElement(kGameOverMenu);
        GameOverMenu.Instance.Init();
    }

    public override void StateEnd()
    {
        World.Instance.RefreshWorld();
        UIManager.HideUiElement(kGameOverMenu);
    }
}

[System.Serializable]
public class PlayGameState : BaseGameState
{
    public Image whiteOverlay;

    private const string kGameHudMenu = "InGameHud";
    private const string kSaveMeMenu = "SaveMeMenu";

    private float gameOverDelay = 1.0f;
    private bool updateWorld = false;
    private bool updateCharacter = false;

    public override void StateStart()
    {
        World.Instance.InitializeWorld();
        Character.Instance.Initialize(GameState.PlayGame);
        DashCourse.Instance.CreateZoneSequence();

        updateWorld = false;
        updateCharacter = false;

        StartWorld();
    }

    private void StartWorld()
    {
        UIManager.ShowUiElement(kGameHudMenu);

        updateWorld = true;
        updateCharacter = true;

        DashCourse.Instance.StartZoneSequence();
    }

    public override void StateUpdate(float deltaTime)
    {
        // if (updateWorld)
        //  World.Instance.UpdateGameWorld();
        if (updateCharacter)
            Character.Instance.UpdateControls();
    }

    public override void StateEnd()
    {
        UIManager.HideUiElement(kGameHudMenu);
        UIManager.HideUiElement(kSaveMeMenu);
    }

    public void RefreshGame()
    {
        UIManager.HideUiElement(kSaveMeMenu);

        Sequence sequence = DOTween.Sequence();

        whiteOverlay.color = Color.white;
        sequence.AppendCallback(() => World.Instance.RefreshWorld());
        sequence.AppendInterval(0.5f);
        sequence.AppendCallback(() => Character.Instance.Initialize(GameState.PlayGameAfterSave));
        sequence.Append(whiteOverlay.DOFade(0f, 0.3f));
        sequence.AppendCallback(() => StartWorld());
    }

    public void GameOver()
    {
        if (updateCharacter && updateWorld)
        {
            updateWorld = false;
            updateCharacter = false;
            DashCourse.Instance.EndZoneSequence();
            World.Instance.CancelZone();
            GameManager.StartCoroutine(DoGameOver());
        }
      
    }

    public IEnumerator DoGameOver()
    {
        yield return new WaitForSeconds(Character.Instance.GetAnimationLength());
        SaveMeMenu.Instance.StartSaveCounter(OnFinishedSaveMeTime);
        UIManager.ShowUiElement(kSaveMeMenu);

    }

    public void OnFinishedSaveMeTime()
    {
        GameManager.ChangeState(GameState.GameOver);
    }
}

