﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGameState
{
    public GameState id;
    public GameManager gameManager;

    protected GameManager GameManager => gameManager;

    public BaseGameState Initialize(GameState uid, GameManager gm)
    {
        id = uid;
        gameManager = gm;
        return this;
    }

    public virtual void StateStart()
    {
    }

    public virtual void StateUpdate(float delta)
    {

    }

    public virtual void StateEnd()
    {

    }
}
