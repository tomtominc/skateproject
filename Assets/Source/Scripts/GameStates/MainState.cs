﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;

[System.Serializable]
public class MainState : BaseGameState
{
    public override void StateStart()
    {
        UIManager.ShowUiElement("MainMenu");
        Character.Instance.Initialize(GameState.Main);
    }

    public override void StateEnd()
    {
        UIManager.HideUiElement("MainMenu");
    }
}
