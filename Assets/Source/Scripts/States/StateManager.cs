﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager
{
    private GameManager context;
    public BaseGameState currState;
    private Dictionary<GameState, BaseGameState> states;

    public StateManager(GameManager context)
    {
        this.context = context;
        states = new Dictionary<GameState, BaseGameState>();
    }

    public void AddState(BaseGameState state)
    {
        states.Add(state.id, state);
    }

    public void Update(float delta)
    {
        if (currState != null && states.ContainsKey(currState.id))
        {
            states[currState.id].StateUpdate(delta);
        }
    }

    public void ChangeState(GameState state)
    {
        if (currState != null && states.ContainsKey(currState.id))
        {
            states[currState.id].StateEnd();
        }

        currState = states[state];

        if (currState != null && states.ContainsKey(currState.id))
        {
            states[currState.id].StateStart();
        }
    }
}
