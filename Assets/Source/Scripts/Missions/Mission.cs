﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MissionType
{
    CollectInRun,
    CollectTotal,
}

public enum CollectType
{
    Currency,
    Special,
    Item,
    Meters,
    Grind,
}

[System.Serializable]
public class Mission
{
    public string key;

    public MissionType missionType;
    public CollectType collectType;

    public int progress;
}
