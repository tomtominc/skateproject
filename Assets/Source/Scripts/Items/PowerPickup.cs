﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPickup : Pickup
{
    public PowerType powerType;

    public override void Collect()
    {
        base.Collect();
        Character.Instance.ActivatePower(powerType);
    }
}
