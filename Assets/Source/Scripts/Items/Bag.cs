﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

using System.Linq;
using System;

public class Bag : Singleton<Bag >
{
    public ItemDatabase itemDatabase;

    private void OnEnable()
    {
        itemDatabase.Save();
        itemDatabase.Load();
    }

    private void OnDisable()
    {
        itemDatabase.Save();
    }

    public static void Remove(ItemKey key, int amount)
    {
        Item item = Instance.itemDatabase.Find((int)key);
        item.Value = (int)Mathf.Clamp(item.Value - amount, 0, Mathf.Infinity);
        item.SessionValue = (int)Mathf.Clamp(item.SessionValue - amount, 0, Mathf.Infinity);
    }

    public static void Add(ItemKey key, int amount)
    {
        Item item = Instance.itemDatabase.Find((int)key);

        if (item == null)
        {
            Debug.LogFormat("No item with ID, {0}", key);
            return;
        }
        item.Value = (int)Mathf.Clamp(item.Value + amount, 0, Mathf.Infinity);
        item.SessionValue = (int)Mathf.Clamp(item.SessionValue + amount, 0, Mathf.Infinity);
        item.LifetimeValue = (int)Mathf.Clamp(item.LifetimeValue + amount, 0, Mathf.Infinity);
    }

    public static void SetBestValue(ItemKey key, int bestValue)
    {
        Item item = Instance.itemDatabase.Find((int)key);
        item.BestValue = bestValue;
    }

    public static void SetSessionValue(ItemKey key, int sessionAmount)
    {
        Item item = Instance.itemDatabase.Find((int)key);
        item.SessionValue = sessionAmount;
    }

    public static Item Get(ItemKey key)
    {
        return Instance.itemDatabase.Find((int)key);
    }


}

public enum ItemKey
{
    None = 0,
    Coins = 1,
    Scraps = 2,
    Gems = 3,
    Hotdog = 4,
    Sushi = 5,
    Pizza = 6,
    Bronze = 7,
    Silver = 8,
    Gold = 9,
    Meters = 10,
    Grinds = 11,
    GameOvers = 13,
    Critter1 = 14,
    Critter2 = 15,
    Critter3 = 16
}
