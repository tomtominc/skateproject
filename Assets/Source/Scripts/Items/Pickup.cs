﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public interface IBuildComponent
{
    void Refresh();
}

[RequireComponent(typeof(SpriteAnimation))]
public class Pickup : MonoBehaviour, IBuildComponent
{
    public ItemKey itemKey;
    public int value = 1;
    public LayerMask collisionLayers;
    public AudioKey audioKey;
    public string normalAnimation = "Normal";
    public string collectedAnimation = "Collected";

    protected SpriteAnimation spriteAnimation;
    protected SpriteRenderer spriteRenderer;
    protected Vector3 originalPosition;
    protected bool init = false;
    protected bool collected = false;

    public virtual int Value
    {
        get { return value; }
    }

    public virtual void Collect()
    {
        if (!collected)
        {
            collected = true;

            spriteAnimation.PlayWithCallBack(collectedAnimation, (x, y) =>
                {
                    gameObject.SetActive(false);
                });

            //AudioStorage.Instance.Play(audioKey, 4);

            Bag.Add(itemKey, Value);
        }
    }

    public virtual void Refresh()
    {
        if (!init)
        {
            spriteAnimation = GetComponent < SpriteAnimation >();
            spriteRenderer = GetComponent < SpriteRenderer >();
            originalPosition = transform.localPosition;

            init = true;
        }

        transform.localPosition = originalPosition;
        collected = false;
        gameObject.SetActive(true);
        spriteAnimation.Play(normalAnimation);
    }
}
