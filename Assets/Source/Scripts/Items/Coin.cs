﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public class Coin : Pickup
{
    private float minDistance = 5f;
    private float speed = 1.1f;

    public override int Value
    {
        get
        {
            if (Character.Instance.IsPowerActive(PowerType.CoinDoubler))
            {
                return value * 2;
            }

            return value;
        }
    }

    public override void Refresh()
    {
        base.Refresh();
        if (Character.Instance.IsPowerActive(PowerType.CoinDoubler))
        {
            spriteRenderer.color = Color.red;
        }

    }

    private void Update()
    {
        if (Character.Instance.IsPowerActive(PowerType.Magnet))
        {
            float distance = Vector3.Distance(Character.Instance.transform.position, transform.position);

            if (distance >= minDistance)
                return;
            
            transform.position = Vector3.MoveTowards(transform.position, 
                Character.Instance.transform.position, (minDistance - distance) * speed * Time.deltaTime);
        }
        else if (transform.localPosition != originalPosition)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, originalPosition, speed * Time.deltaTime);
        }


    }
}
