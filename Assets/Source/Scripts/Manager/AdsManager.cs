﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_ANDROID || UNITY_IOS
using UnityEngine.Advertisements;
#endif
using UnityEngine.Rendering;
using Framework;


public class AdsManager : Singleton < AdsManager >, IStateListener
{
    private const string RewardVideo = "rewardedVideo";

    public int deathAdCount = 2;
    public int sessionAdShownCount = 0;
    private int currentDeathCount = 0;

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
        currentDeathCount = 0;
        sessionAdShownCount = 0;
    }

    public void ShowAd()
    {
        #if UNITY_ANDROID || UNITY_IOS
        if (Advertisement.IsReady())
        {
            Advertisement.Show();

            sessionAdShownCount++;
            EventManager.Publish(this, GameEvent.AdShown, sessionAdShownCount);
        }
        #endif
    }

    public void ShowRewardedAd(AdRewardType type, Action<AdvertisementData> onFinish)
    {
        #if UNITY_ANDROID || UNITY_IOS
        if (Advertisement.IsReady(RewardVideo))
        {
            var options = new ShowOptions();

            options.resultCallback = (result) =>
            {
                var data = new AdvertisementData { type = type, finished = result == ShowResult.Finished };
                EventManager.Publish(this, GameEvent.RewardAdShown, data);

                if (onFinish != null)
                    onFinish(data);
            };

            Advertisement.Show(RewardVideo, options);
        }
        #endif
    }

    // IStateListener
    public void OnStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.GameOver:
                OnGameOver();
                break;
        }
    }

    private void OnGameOver()
    {
        currentDeathCount++;

        if (currentDeathCount > deathAdCount)
        {
            ShowAd();
            currentDeathCount = 0;
        }
    }

    public struct AdvertisementData
    {
        public bool finished;
        public AdRewardType type;
    }

    public enum AdRewardType
    {
        Currency,
        DoubledIt,
        SaveMe,
        Bonus
    }

}


