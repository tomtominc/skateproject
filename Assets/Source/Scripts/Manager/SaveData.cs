﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class SaveData : SingletonBehaviour < SaveData >
{
    public const string GameDataSave = "GameDataSave";
    public bool hasLoaded = false;
    public GameData gameData;

    private void OnEnable()
    {
        Load();
        hasLoaded = true;
    }

    private void OnDisable()
    {
        Save();
    }

    private void Load()
    {
        string dataString = PlayerPrefs.GetString(GameDataSave, JsonConvert.SerializeObject(new GameData()));
        gameData = JsonConvert.DeserializeObject < GameData >(dataString);
    }

    private void Save()
    {
        PlayerPrefs.SetString(GameDataSave, JsonConvert.SerializeObject(gameData));
    }
}

[SerializeField]
public class GameData
{
    public int critter = 0;
    public int track = 0;
}
