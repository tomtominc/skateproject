﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using Com.LuisPedroFonseca.ProCamera2D.TopDownShooter;
using System.Linq;
using RutCreate.LightningDatabase;

public enum GameState
{
    Main,
    Upgrade,
    Mission,
    OpenChest,
    PlayGame,
    GameOver,
    PlayGameAfterSave

}

public class GameManager : Singleton<GameManager>
{
    public GameState currentGameState;
    public float loadTime = 0.1f;
    public CameraShake cameraShaker;

    [Header("Game State Definitions")]
    public MainState mainState;
    public UpgradeState upgradeState;
    public MissionState missionState;
    public OpenChestState openChestState;
    public PlayGameState playGameState;
    public GameOverState gameOverState;

    internal StateManager stateManager;

    internal List < IStateListener > stateListeners;

    private IEnumerator Start()
    {
        stateListeners = GetComponentsInChildren < IStateListener >().ToList();

        stateManager = new StateManager(this);

        stateManager.AddState((MainState)mainState.Initialize(GameState.Main,this));
        stateManager.AddState((UpgradeState)upgradeState.Initialize(GameState.Upgrade,this));
        stateManager.AddState((MissionState)missionState.Initialize(GameState.Mission,this));
        stateManager.AddState((OpenChestState)openChestState.Initialize(GameState.OpenChest,this));
        stateManager.AddState((PlayGameState)playGameState.Initialize(GameState.PlayGame,this));
        stateManager.AddState((GameOverState)gameOverState.Initialize(GameState.GameOver,this));

        yield return new WaitForEndOfFrame();

        ChangeState(currentGameState);
    }

    private void Update()
    {
        if (stateManager.currState != null)
            stateManager.Update(Time.deltaTime);
    }

    public void ChangeState(GameState state)
    {
        switch (state)
        {
            case GameState.PlayGame:
                Bag.SetSessionValue(ItemKey.Coins, 0);
                Bag.SetSessionValue(ItemKey.Meters, 0);
                break;

            case GameState.GameOver:

                Item coins = Bag.Get(ItemKey.Coins);
                Item meters = Bag.Get(ItemKey.Meters);

                meters.SessionValue = (int)World.Instance.score;

                Bag.SetBestValue(ItemKey.Coins, Mathf.Max(coins.BestValue,
                        coins.SessionValue));

                Bag.SetBestValue(ItemKey.Meters, Mathf.Max(meters.BestValue,
                        meters.SessionValue));
                break;
        }

        Debug.Log($"Change State {state}");
        currentGameState = state;
        stateManager.ChangeState(currentGameState);
        stateListeners.ForEach(x => x.OnStateChanged(state));
    }

    public void StartGame()
    {
        ChangeState(GameState.PlayGame);
    }

    public void RefreshGame()
    {
        if (currentGameState == GameState.PlayGame)
        {
            ((PlayGameState)stateManager.currState).RefreshGame();
        }
    }

    public void GameOver()
    {
        EventManager.Publish(this, GameEvent.GameOver, null);

        Debug.Log($"Current State = {currentGameState}");

        if (currentGameState == GameState.PlayGame)
        {
            ((PlayGameState)stateManager.currState).GameOver();
        }
    }

    public void MainMenu()
    {
        ChangeState(GameState.Main);
    }

    public void ShakeCamera()
    {
        cameraShaker.Shake();
    }

    public void ShakeCamera(Vector3 direction)
    {
        cameraShaker.Shake(CameraShake.ShakeType.CameraMatrix, 2,
            direction, new Vector3(0f, 0f, 0f), 0.1f, 50f, 0.2f, 1f, true);
    }
}
