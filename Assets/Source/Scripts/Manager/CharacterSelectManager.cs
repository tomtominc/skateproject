﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RutCreate.LightningDatabase;

public class CharacterSelectManager : MonoBehaviour
{
    public Button playGameButton;
    public List < SpriteRenderer > critters;

    private void Start()
    {
        SwapCritter(SaveData.Instance.gameData.critter);
    }

    public void SwapCritter(int critterIndex)
    {
        for (int i = 0; i < critters.Count; i++)
        {
            var critter = critters[i];
            critter.material.SetFloat("_Val", 0);
        }

        critters[critterIndex].material.SetFloat("_Val", 1);
      
        SaveData.Instance.gameData.critter = critterIndex;

        Item selectedCritter = Bag.Get(((ItemKey)14 + critterIndex));

        Debug.Log(selectedCritter.Name);
        if (selectedCritter.Value > 0)
        {
            
            playGameButton.interactable = true;
            playGameButton.transform.Find("CritterLockedPanel").gameObject.SetActive(false);
        }
        else
        {
            playGameButton.interactable = false;
            playGameButton.transform.Find("CritterLockedPanel").gameObject.SetActive(true);
        }
    }
}
