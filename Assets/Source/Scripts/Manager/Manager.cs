﻿using System.Collections.Generic;
using Framework;
using UnityEngine.Analytics;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

public enum GameEvent
{
    /// <summary>
    /// The Game Over Event:
    /// Data Sent:
    /// None.
    /// </summary>
    GameOver,
    /// <summary>
    /// The reward ad shown.
    /// Data Sent:
    /// ShowResult Enum
    /// </summary>
    RewardAdShown,
    /// <summary>
    /// The ad shown.
    /// </summary>
    AdShown,
    /// <summary>
    /// The run started.
    /// </summary>
    RunStarted,
    /// <summary>
    /// The run ended.
    /// </summary>
    RunEnded
}

public class Manager : PersistentSingleton<Manager>
{

    public bool logConsole = true;
    public bool logEvents = true;

    public Text consoleLabel;

    private void OnEnable()
    {
        EventManager.Initialize();
        EventManager.Subscribe(GameEvent.AdShown, OnAdShown);
        EventManager.Subscribe(GameEvent.RewardAdShown, OnRewardAdShown);
        EventManager.Subscribe(GameEvent.GameOver, OnGameOver);
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(GameEvent.AdShown, OnAdShown);
        EventManager.Unsubscribe(GameEvent.RewardAdShown, OnRewardAdShown);
        EventManager.Unsubscribe(GameEvent.GameOver, OnGameOver);
    }

    public string DictToString<T, V>(IEnumerable<KeyValuePair<T, V>> items, string format)
    {
        format = string.IsNullOrEmpty(format) ? "<color=yellow>{0}</color>='<color=white>{1}</color>' " : format;

        StringBuilder itemString = new StringBuilder();

        foreach (var item in items)
            itemString.AppendFormat(format, item.Key, item.Value);

        return itemString.ToString();
    }

    private void LogEvent(string format, object e, Dictionary<string,object> args)
    {
        if (logConsole)
            Debug.LogFormat(format + "\n", e, DictToString(args, string.Empty));

        if (logEvents)
        {
            StopCoroutine("ClearConsole");
            consoleLabel.text += string.Format(format + "\n", e, DictToString(args, string.Empty));
            StartCoroutine("ClearConsole");
        }
    }

    private IEnumerator ClearConsole()
    {
        yield return new WaitForSeconds(5f);
        consoleLabel.text = string.Empty;
    }

    private void CustomEvent(object e, Dictionary<string,object> values)
    {
        Analytics.CustomEvent(e.ToString(), values);
        LogEvent("(Event)<color=red>{0}</color>: {1}", e, values);
    }

    private void OnAdShown(IMessage message)
    {
        int shownAdCount = (int)message.Data;

        var values = new Dictionary < string, object >
        {
            { "Session Count", shownAdCount }
        };

        CustomEvent(GameEvent.AdShown, values);
    }

    private void OnRewardAdShown(IMessage message)
    {
        AdsManager.AdvertisementData data = (AdsManager.AdvertisementData)message.Data;

        var values = new Dictionary < string, object >
        {
            { "Finished", data.finished },
            { "Reward", data.type }

        };

        CustomEvent(GameEvent.RewardAdShown, values);
    }

    private void OnGameOver(IMessage message)
    {
        var values = new Dictionary < string, object >
        {
            { "Coins", Bag.Get(ItemKey.Coins).SessionValue },
            { "Total Coins", Bag.Get(ItemKey.Coins).Value },
            { "Meters", Bag.Get(ItemKey.Meters).SessionValue }
        };

        CustomEvent(GameEvent.GameOver, values);
    }
}


