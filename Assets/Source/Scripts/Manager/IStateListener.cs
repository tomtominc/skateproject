﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStateListener
{
    void OnStateChanged(GameState state);
}
