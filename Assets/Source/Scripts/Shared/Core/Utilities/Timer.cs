﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

namespace Framework
{
    public static class Timer
    {
        public static void DoAction(this MonoBehaviour behaviour, float delay, Action action)
        {
            behaviour.StartCoroutine(DoAction(delay, action));
        }

        public static void DoActionIndependentTimeScale(this MonoBehaviour behaviour, float delay, Action action)
        {
            Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(delay);
            sequence.AppendCallback(() => action.Invoke());
            sequence.SetUpdate(true);
            sequence.Play();
        }

        public static IEnumerator DoAction(float delay, Action action)
        {
            yield return new WaitForSeconds(delay);
            action.Invoke();
        }
    }
}

