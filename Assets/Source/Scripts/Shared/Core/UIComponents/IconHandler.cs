﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class IconHandler : MonoBehaviour , IPointerDownHandler, IPointerUpHandler, 
                                        IPointerExitHandler, IPointerEnterHandler
{
    public float onDownOffset = 3f;

    private List < RectTransform > elements;
    private List < Vector2 > positions;

    private void Start()
    {
        elements = GetComponentsInChildren < RectTransform >(true).ToList();
        elements.Remove(GetComponent < RectTransform >());
        positions = elements.Select(x => x.anchoredPosition).ToList();
    }

    public void OnPointerDown(PointerEventData data)
    {
        foreach (var element in elements)
        {
            var p0 = element.anchoredPosition;
            p0.y = p0.y - onDownOffset;
            element.anchoredPosition = p0;
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        for (int i = 0; i < elements.Count; i++)
        {
            elements[i].anchoredPosition = positions[i];
        }
    }

    public void OnPointerExit(PointerEventData data)
    {
        for (int i = 0; i < elements.Count; i++)
        {
            elements[i].anchoredPosition = positions[i];
        }
    }

    public void OnPointerEnter(PointerEventData data)
    {
        if (data.pointerPress != gameObject)
            return;
        
        foreach (var element in elements)
        {
            var p0 = element.anchoredPosition;
            p0.y = p0.y - onDownOffset;
            element.anchoredPosition = p0;
        }
    }
}
