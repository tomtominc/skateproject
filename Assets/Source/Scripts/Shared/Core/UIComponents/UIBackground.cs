﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBackground : MonoBehaviour
{
    [SerializeField] protected bool m_scroll;
    [SerializeField] protected float m_speed;
    [SerializeField] protected Vector2 m_direction;

    protected Material m_material;

    private void Start()
    {
        Image l_image = GetComponent < Image >();
        SpriteRenderer l_spriteRenderer = GetComponent < SpriteRenderer >();

        if (null != l_image)
            m_material = l_image.material;

        if (null != l_spriteRenderer)
            m_material = l_spriteRenderer.material;

        resetBackground();
    }

    private void OnDisable()
    {
        resetBackground();
    }

    private void Update()
    {
        if (m_scroll)
            scrollBackground(m_direction * m_speed * Time.deltaTime);
    }

    public void scrollBackground(Vector2 p_velocity)
    {
        m_material.mainTextureOffset += p_velocity;
    }

    public void resetBackground()
    {
        m_material.mainTextureOffset = Vector2.zero;
    }
}
