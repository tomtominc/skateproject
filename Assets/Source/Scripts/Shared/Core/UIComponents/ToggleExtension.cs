﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class ToggleExtension : MonoBehaviour
{
    protected ToggleGroupExtension m_toggleGroup;
    protected Toggle m_toggle;

    public virtual void init(ToggleGroupExtension p_navigationView)
    {
        m_toggleGroup = p_navigationView;
        m_toggle = GetComponent < Toggle >();
        m_toggle.onValueChanged.AddListener(toggle);
    }

    public void forceToggle(bool isOn)
    {
        m_toggle.isOn = isOn;
    }

    protected void toggle(bool isOn)
    {
        if (isOn)
        {
            onToggleOn();
            m_toggleGroup.toggleTriggered(this);
        } 
    }

    public virtual void onToggleOn()
    {
    }

    public virtual void onToggleOff()
    {
    }

}
