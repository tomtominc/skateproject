﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using System.Linq;

public abstract class ApplicationManager<T> : SerializedMonoBehaviour where T : IConvertible
{
    #region Memebers

    // [SerializeField][BoxGroup("Application Injections")]
    // protected GameServiceProvider m_gameServiceProvider;
    [SerializeField][BoxGroup("Application Injections")]
    protected SceneController m_sceneController;

    [SerializeField]
    protected Dictionary < string, ApplicationState<T> > m_applicationStates;

    protected T m_currentState;

    #endregion

    #region Properties

    protected string m_currentStateName
    {
        get { return m_currentState.ToString(); }
    }

    protected List<ApplicationState<T>> m_stateList
    {
        get { return m_applicationStates.Values.ToList(); }
    }

    // public GameServiceProvider gameServiceProvider
    // {
    //     get { return m_gameServiceProvider; }
    // }

    public SceneController sceneController
    {
        get { return m_sceneController; }
    }

    #endregion

    #region Unity Hooks

    private void Awake()
    {
        //m_gameServiceProvider.Initialize();
        applicationInitializeStates();
    }

    private void Start()
    {
        applicationEnter();
        applicationEnterInitialState();
    }

    private void Update()
    {
        applicationUpdate();
        applicationUpdateState();
    }

    private void OnApplicationQuit()
    {
        applicationQuit();
    }

    #endregion

    #region Abstract & Virtual Methods

    protected abstract void applicationEnter();

    protected abstract void applicationUpdate();

    protected abstract void applicationQuit();

    protected virtual void applicationInitializeStates()
    {
        m_stateList.ForEach(appState => appState.initialize(this));
    }

    protected virtual void applicationEnterInitialState()
    {
        if (m_applicationStates.ContainsKey(m_currentStateName))
            m_applicationStates[m_currentStateName].enter();
    }

    protected virtual void applicationUpdateState()
    {
        if (m_applicationStates.ContainsKey(m_currentStateName))
            m_applicationStates[m_currentStateName].update();
    }

    public virtual void applicationChangeState(T p_state)
    {
        if (m_applicationStates.ContainsKey(m_currentStateName))
            m_applicationStates[m_currentStateName].exit();

        m_currentState = p_state;

        if (m_applicationStates.ContainsKey(m_currentStateName))
            m_applicationStates[m_currentStateName].enter();
    }

    #endregion

}
