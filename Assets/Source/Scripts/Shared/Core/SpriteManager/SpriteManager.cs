﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class SpriteManager : SingletonBehaviour < SpriteManager >
{
    [SerializeField]
    private SpriteAtlas m_atlas;

    public static Dictionary < string , Sprite > m_spriteMap;

    private static void configureMap()
    {
        m_spriteMap = new Dictionary<string, Sprite>();
        Sprite[] l_sprites = new Sprite[Instance.m_atlas.spriteCount];

        Instance.m_atlas.GetSprites(l_sprites);

        for (int i = 0; i < l_sprites.Length; i++)
        {
            Sprite l_sprite = l_sprites[i];
            l_sprite.name = l_sprite.name.Replace("(Clone)", "");

            if (!m_spriteMap.ContainsKey(l_sprite.name))
            {
                m_spriteMap.Add(l_sprite.name, l_sprite);
            }
        }

    }

    public static Sprite getSprite(string p_name)
    {
        if (m_spriteMap == null)
            configureMap();

        if (!m_spriteMap.ContainsKey(p_name))
        {
            Debug.LogWarningFormat("Couldn't find sprite with name {0}", p_name);
            return null;
        }

        return m_spriteMap[p_name];
    }

}
