﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class EventManager
{
    private static Dictionary<System.Enum, MessageHandler> _messageHandlers;

    public static void Initialize()
    {
        _messageHandlers = new Dictionary<System.Enum, MessageHandler>();
    }

    public static void ClearListeners()
    {
        _messageHandlers.Clear();
    }

    public static void Subscribe(System.Enum pMessage, MessageHandler pHandler)
    {
        MessageListenerDefinition lListener = MessageListenerDefinition.allocate();
        lListener.messageType = pMessage;
        lListener.handler = pHandler;
        Subscribe(lListener);
    }

    private static void Subscribe(MessageListenerDefinition pListener)
    {
        if (_messageHandlers.ContainsKey(pListener.messageType))
        {
            _messageHandlers[pListener.messageType] += pListener.handler;
        }
        else
        {
            _messageHandlers.Add(pListener.messageType, pListener.handler);
        }

        MessageListenerDefinition.release(pListener);
    }

    public static void Unsubscribe(System.Enum pMessage, MessageHandler pHandler)
    {
        MessageListenerDefinition lListener = MessageListenerDefinition.allocate();
        lListener.messageType = pMessage;
        lListener.handler = pHandler;
        Unsubscribe(lListener);
    }

    private static void Unsubscribe(MessageListenerDefinition pListener)
    {
        if (_messageHandlers.ContainsKey(pListener.messageType))
        {
            MessageHandler lHandler = _messageHandlers[pListener.messageType];
            lHandler -= pListener.handler;

            if (null == lHandler)
                _messageHandlers.Remove(pListener.messageType);
        }
        MessageListenerDefinition.release(pListener);
    }

    public static void Publish(object pSender, System.Enum pMessage, object pData)
    {
        Message lMessage = Message.allocate();
        lMessage.Sender = pSender;
        lMessage.MessageType = pMessage;
        lMessage.Data = pData;

        Publish(lMessage);

        lMessage.Release();
    }

    private static void Publish(IMessage pMessage)
    {
        if (_messageHandlers.ContainsKey(pMessage.MessageType))
        {
            MessageHandler lHandler = _messageHandlers[pMessage.MessageType];
            lHandler(pMessage);
        }
    }
}

public delegate void MessageHandler(IMessage rMessage);

