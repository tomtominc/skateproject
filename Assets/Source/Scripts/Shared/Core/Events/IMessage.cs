﻿
public interface IMessage
{
    System.Enum MessageType { get; set; }

    object Sender { get; set; }

    object Data { get; set; }

    void Clear();

    void Release();
}

