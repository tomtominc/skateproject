﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventDispatcher : MonoBehaviour
{
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        EventManager.Initialize();
    }

    public void OnDisable()
    {
        EventManager.ClearListeners();
    }
}
