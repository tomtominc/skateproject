﻿
public class Message : IMessage
{
    protected System.Enum m_messageType;

    public System.Enum MessageType
    {
        get { return m_messageType; }
        set { m_messageType = value; }
    }

    protected object m_sender;

    public object Sender
    {
        get { return m_sender; }
        set { m_sender = value; }
    }

    protected object m_data;

    public object Data
    {
        get { return m_data; }
        set { m_data = value; }
    }

    public virtual void Clear()
    {
        m_sender = null;
        m_data = null;
    }

    public virtual void Release()
    {
        Clear();
        m_pool.release(this);
    }

    // ******************************** OBJECT POOL ********************************

    private static ObjectPool<Message> m_pool = new ObjectPool<Message>(40, 10);

    public static Message allocate()
    {
        Message l_instance = m_pool.allocate();
        if (l_instance == null)
        {
            l_instance = new Message();
        }
        return l_instance;
    }

    public static void release(Message p_message)
    {
        if (p_message == null)
        {
            return;
        }

        m_pool.release(p_message);
    }
}
