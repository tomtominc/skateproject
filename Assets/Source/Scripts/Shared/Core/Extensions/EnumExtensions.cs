﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace Framework
{
    public static class Enum < T > where T : struct, IConvertible
    {
        public static T Parse(string name)
        {
            if (!Enum.IsDefined(typeof(T), name))
                return default(T);
                
            return (T)Enum.Parse(typeof(T), name);   
        }

        public static List < string > ToList()
        {
            return Enum.GetNames(typeof(T)).ToList();
        }

        public static List < T > ToValues()
        {
            return Enum.GetValues(typeof(T)).Cast <T>().ToList();
        }

       
       
    }

    public static class EnumExtensions
    {
        public static T Next<T>(this T value) where T : struct, IConvertible
        {
            return Enum.GetValues(typeof(T)).Cast<T>().FirstOrDefault(x => (Convert.ToInt32(x) > Convert.ToInt32(value))); 
        }

        public static int LengthOf<T>(this T value)where T : struct, IConvertible
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList().Count; 
        }

        public static T Move<T>(this T value, int steps)where T : struct, IConvertible
        {
            int totalSteps = Mathf.Abs(steps);
            int direction = (int)Mathf.Sign((float)steps);
            int enumCount = value.LengthOf() - 1;
            int currentValue = Convert.ToInt32(value);

            for (int i = 0; i < totalSteps; i++)
            {
                currentValue += (1 * direction);

                if (currentValue > enumCount)
                {
                    currentValue = 0;
                    direction *= -1;
                }

                if (currentValue < 0)
                {
                    currentValue = enumCount;
                    direction *= -1;
                }
            }

            return (T)(object)currentValue;
        }

    }

   
}

public static class EnumExtensions
{
    public static T Parse < T >(string name) where T : struct, IConvertible
    {
        if (!Enum.IsDefined(typeof(T), name))
            return default(T);

        return (T)Enum.Parse(typeof(T), name);   
    }

    public static List < string > ToList < T >() where T : struct, IConvertible
    {
        return Enum.GetNames(typeof(T)).ToList();
    }


    public static List < T > ToValues < T >() where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).Cast <T>().ToList();
    }

    public static bool IsFirst <T>(this T value) where T : struct, IConvertible
    {
        return Convert.ToInt32(value) <= 0;
    }

    public static bool IsLast <T>(this T value) where T : struct, IConvertible
    {
        return Convert.ToInt32(value) >= value.LengthOf() - 1;
    }

    public static T NextStep < T >(this T value) where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).Cast<T>().FirstOrDefault(x => Convert.ToInt32(x) > Convert.ToInt32(value));
    }

    public static int LengthOf<T>(this T value)where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).Cast<T>().ToList().Count; 
    }

    public static int ToInt<T>(this T value) where T : struct, IConvertible
    {
        return Convert.ToInt32(value);
    }

    public static int GetMaxSetFlagValue<T>(T flags) where T : struct, IConvertible
    {
        int value = (int)Convert.ChangeType(flags, typeof(int));
        IEnumerable<int> setValues = Enum.GetValues(flags.GetType()).Cast<int>().Where(f => (f & value) == f);
        return setValues.Any() ? setValues.Max() : 0;
    }

    public static T Move<T>(this T value, int steps)where T : struct, IConvertible
    {
        int totalSteps = Mathf.Abs(steps);
        int direction = (int)Mathf.Sign((float)steps);
        int enumCount = value.LengthOf() - 1;
        int currentValue = Convert.ToInt32(value);

        for (int i = 0; i < totalSteps; i++)
        {
            currentValue += (1 * direction);

            if (currentValue > enumCount)
            {
                currentValue = 0;
                direction *= -1;
            }

            if (currentValue < 0)
            {
                currentValue = enumCount;
                direction *= -1;
            }
        }

        return (T)(object)currentValue;
    }

    public static bool HasFlag(this Enum variable, Enum value)
    {
        ulong num = Convert.ToUInt64(value);
        ulong num2 = Convert.ToUInt64(variable);

        return (num2 & num) == num;
    }
}
