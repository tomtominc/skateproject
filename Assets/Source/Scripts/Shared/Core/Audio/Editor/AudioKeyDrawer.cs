﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomPropertyDrawer(typeof(AudioKey))]
public class AudioKeyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {    
        property.serializedObject.Update();

        SerializedProperty value = property.FindPropertyRelative("value");

        string[] keys = AudioManager.Instance.m_storage.Select(x => x.key).ToArray();

        value.intValue = EditorGUI.Popup(position, property.displayName, value.intValue, keys);

        property.serializedObject.ApplyModifiedProperties();

    }
}
