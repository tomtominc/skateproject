﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WorldSegmentContainer))]
public class WorldZone : MonoBehaviour
{
    public enum Frame
    {
        Entry,
        Main0,
        Main1,
        Main2,
        End
    }

    public Zone zone;
    public Frame currentFrame;
    public Dictionary < Frame, WorldFrameBehaviour > frames;


    protected World world;
    protected WorldSegmentContainer container;
    protected SegmentMoveOptions options;

    public virtual bool IsComplete
    {
        get { return true; }
    }

    public virtual bool HasCompleteCondition
    {
        get { return false; }
    }

    public virtual void Init(World world)
    {
        this.world = world;
        this.container = GetComponent < WorldSegmentContainer >().Initialize();
        this.options = new SegmentMoveOptions();

    }

    public virtual void StartScene()
    {
        ChangeFrame(Frame.Entry);
    }

    public virtual void UpdateWorld()
    {
        if (frames.ContainsKey(currentFrame))
        {
            WorldFrameBehaviour behaviour = frames[currentFrame];

            options.zone = zone;
            options.segmentType = behaviour.SegmentType;
            options.segmentId = behaviour.SegmentId;
            options.speedOverride = behaviour.SpeedOverride;
        }

        world.UpdateMover(container, options);
    }

    public virtual void UpdateScene()
    {
        if (frames.ContainsKey(currentFrame))
            frames[currentFrame].UpdateFrame();

        UpdateWorld();
    }

    public virtual void EndScene()
    {
        if (frames.ContainsKey(currentFrame))
            frames[currentFrame].EndFrame();

        ChangeFrame(Frame.Entry);
    }

    public virtual void CancelScene()
    {
        if (frames.ContainsKey(currentFrame))
            frames[currentFrame].CancelFrame();
    }

    public void Refresh()
    {
        container.Refresh();
    }

    public void ChangeFrame(Frame frame)
    {
        if (frames.ContainsKey(currentFrame))
            frames[currentFrame].EndFrame();

        currentFrame = frame;

        if (frames.ContainsKey(currentFrame))
            frames[currentFrame].StartFrame();
    }

    public virtual bool IsBonusReady
    {
        get { return false; }
    }

    public virtual bool IsValidBonusArea
    {
        get { return false; }
    }


}