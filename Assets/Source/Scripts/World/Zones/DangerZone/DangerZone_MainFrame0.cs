﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

[System.Serializable]
public class DangerZone_MainFrame0 : WorldFrameBehaviour
{
    public float startDelay = 2f;
    public float totalFrameTime = 10f;

    private float currentTime = 0f;

    public override string SegmentId
    {
        get { return "Electric"; }
    }

    public override SegmentType SegmentType
    {
        get { return SegmentType.Main; }
    }

    public override void StartFrame()
    {
        currentTime = 0;
    }

    public override void UpdateFrame()
    {
        currentTime += Time.deltaTime;

        if (currentTime < totalFrameTime)
            return;

        Debug.Log("Changing frames");
        zone.ChangeFrame(WorldZone.Frame.Main1);
           
    }

    public override void CancelFrame()
    {
        
    }
}
