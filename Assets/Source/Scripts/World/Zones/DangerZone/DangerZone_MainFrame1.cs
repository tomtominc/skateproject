﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

[System.Serializable]
public class DangerZone_MainFrame1 : WorldFrameBehaviour
{
    
    public float startDelay = 1.0f;
    public int spawnCount = 20;
    public ObstacleSpawner obstacleSpawner;
   
    public float rechargeTime = 0.5f;
    private bool spawnObstacles = false;

    private int currentSpawnCount = 0;
    private bool isComplete = false;
    private float currentTime = 0f;
    private int spawnChance = 3;

    public bool IsComplete
    {
        get { return isComplete; }
    }

    public override string SegmentId
    {
        get { return "Plain"; }
    }

    public override SegmentType SegmentType
    {
        get{ return SegmentType.Main; }
    }

    public override void StartFrame()
    {
        isComplete = false;
        currentSpawnCount = 0;
        spawnChance = 3;
        spawnObstacles = false;
        currentTime = 0f;
        obstacleSpawner.Collision += OnCollisionEnter;

        DoAction(startDelay, () => spawnObstacles = true);
    }

    public override void UpdateFrame()
    {
        if (!spawnObstacles || Character.Instance.isDead)
            return;

        currentTime += Time.deltaTime;

        if (currentTime < rechargeTime)
            return;

        SpawnRandomObstacles();

        currentSpawnCount++;
        currentTime = 0f;

        if (currentSpawnCount >= spawnCount * 0.5f)
        {
            spawnChance = 9;
        }

        if (currentSpawnCount >= spawnCount)
        {
            spawnObstacles = false;
            isComplete = true;
        }
    }

    int last = -1;

    public void SpawnRandomObstacles()
    {
        int rand = last;

        while (last == rand)
            rand = Random.Range(0, spawnChance);

        last = rand;

        if (rand == 0)
            obstacleSpawner.SpawnObstacles(true, false, false);
        if (rand == 1)
            obstacleSpawner.SpawnObstacles(false, true, true);
        if (rand == 2)
            obstacleSpawner.SpawnObstacles(false, true, false);
        if (rand == 3)
            obstacleSpawner.SpawnObstacles(false, true, false);
        if (rand == 4)
            obstacleSpawner.SpawnObstacles(false, true, true);
        if (rand == 5)
            obstacleSpawner.SpawnObstacles(true, true, false);
        if (rand == 6)
            obstacleSpawner.SpawnObstacles(true, false, true);
        if (rand == 7)
            obstacleSpawner.SpawnObstacles(false, true, true);
        if (rand == 8)
            obstacleSpawner.SpawnObstacles(true, true, false);
    }

    public override void EndFrame()
    {
        isComplete = false;
    }

    public override void CancelFrame()
    {
        isComplete = false;
        obstacleSpawner.Collision -= OnCollisionEnter;
        obstacleSpawner.DumpAssets();
    }

    public void OnCollisionEnter(object sender, WorldObject.CollisionInfo collisionInfo)
    {
        // spawnObstacles = false;
    }
}

[System.Serializable]
public class DangerZone_MainFrame1_Alt0 : WorldFrameBehaviour
{
    public float startDelay = 1.0f;
    public int numberOfZaps = 20;
    public LaneSignals laneSignals;
    public float rechargeTime = 0.5f;
    public CameraShake cameraShake;


    float blinkTime;
    float zapTime;

    public override string SegmentId
    {
        get { return "Plain"; }
    }

    public override SegmentType SegmentType
    {
        get{ return SegmentType.Main; }
    }

    public override void StartFrame()
    {
        blinkTime = laneSignals.blinkWaitTime * (laneSignals.numberOfBlinks * 2f);
        zapTime = 1f - blinkTime;
        zone.StartCoroutine(ZoneActions());
    }

    public IEnumerator ZoneActions()
    {
        yield return new WaitForSeconds(startDelay);

        for (int i = 0; i < numberOfZaps; i++)
        {

            if (Character.Instance.isDead)
            {
                yield break;
            }
           
            DoZapsRandom();
            yield return new WaitForSeconds(blinkTime);
            cameraShake.Shake();
            yield return new WaitForSeconds(zapTime);

        }

    }

    int last = -1;

    public void DoZapsRandom()
    {
        int rand = last;

        while (last == rand)
            rand = Random.Range(0, 3);

        last = rand;

        if (rand == 0)
            laneSignals.TriggerElectricEffect(true, false, true);
        if (rand == 1)
            laneSignals.TriggerElectricEffect(false, true, true);
        if (rand == 2)
            laneSignals.TriggerElectricEffect(true, true, false);
    }
}
