﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DangerZone : WorldZone
{
    public DangerZone_EntryFrame entryFrame;
    public DangerZone_MainFrame0 mainFrame0;
    public DangerZone_MainFrame1 mainFrame1;

    public override bool HasCompleteCondition
    {
        get{ return true; }
    }

    public override bool IsComplete
    {
        get  { return mainFrame1.IsComplete; }
    }

    public override void Init(World world)
    {
        base.Init(world);

        options = new SegmentMoveOptions();

        frames = new Dictionary < Frame , WorldFrameBehaviour >()
        {
            { Frame.Entry, (DangerZone_EntryFrame)entryFrame.Init(this) },
            { Frame.Main0, (DangerZone_MainFrame0)mainFrame0.Init(this) },
            { Frame.Main1, (DangerZone_MainFrame1)mainFrame1.Init(this) }
        };
    }
}
