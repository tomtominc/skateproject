﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DangerZone_EntryFrame : WorldFrameBehaviour
{
    public override SegmentType SegmentType
    {
        get { return SegmentType.Start; }
    }

    public float secondsUntilNextFrame = 2f;

    public override void StartFrame()
    {
        DoAction(secondsUntilNextFrame, () =>
            {
                zone.ChangeFrame(WorldZone.Frame.Main0); 
            });
    }
}
