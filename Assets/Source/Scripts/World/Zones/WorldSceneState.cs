﻿using System;
using System.Collections;
using UnityEngine;


public class WorldFrameBehaviour
{
    protected WorldZone zone;

    public virtual SegmentType SegmentType
    {
        get { return SegmentType.Start; }
    }

    public virtual float SpeedOverride
    {
        get { return 0f; }
    }

    public virtual string SegmentId
    {
        get { return "NULL"; }
    }

    public virtual WorldFrameBehaviour Init(WorldZone zone)
    {
        this.zone = zone;
        return this;
    }

    public virtual void StartFrame()
    {
    }

    public virtual void UpdateFrame()
    {
    }

    public virtual void EndFrame()
    {
    }

    public virtual void CancelFrame()
    {
    }

    protected void DoAction(float delay, Action action)
    {
        zone.StartCoroutine(ActionRoutine(delay, action));
    }

    private IEnumerator ActionRoutine(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action();
    }
}
