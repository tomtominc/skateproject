﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[System.Serializable]
public class GenericZone_EntryFrame : WorldFrameBehaviour
{
    public float secondsUntilNextFrame = 0.5f;

    private float speed;

    public override float SpeedOverride
    {
        get{ return speed; }
    }

    public override void StartFrame()
    {
        zone.ChangeFrame(WorldZone.Frame.Main0);
    }
   
}
