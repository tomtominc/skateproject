﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericZone : WorldZone
{
    public GenericZone_EntryFrame entryFrame;
    public GenericZone_Main0Frame main0Frame;

    public override void Init(World world)
    {
        base.Init(world);

        frames = new Dictionary<Frame, WorldFrameBehaviour>()
        {
            { Frame.Entry, (GenericZone_EntryFrame)entryFrame.Init(this) },
            { Frame.Main0, (GenericZone_Main0Frame)main0Frame.Init(this) }
        };
    }
}
