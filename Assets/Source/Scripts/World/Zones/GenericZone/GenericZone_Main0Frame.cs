﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GenericZone_Main0Frame : WorldFrameBehaviour
{
    public override SegmentType SegmentType
    {
        get { return SegmentType.Main; }
    }
}
