﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(BoxCollider2D))]
public class WorldObject : MonoBehaviour
{
    public bool doesMove = true;

    public bool randomSpeed = false;
    public float speedAdditive = 0f;

    public float speedMin = 0f;
    public float speedMax = 1f;

    public class CollisionInfo : System.EventArgs
    {
        public Collider2D other;
    }

    protected SpriteRenderer spriteRenderer;

    public System.EventHandler < CollisionInfo > OnEnter;

    public void Awake()
    {
        spriteRenderer = GetComponentInChildren < SpriteRenderer >();

        if (randomSpeed)
            speedAdditive = Random.Range(speedMin, speedMax);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (OnEnter != null)
        {
            OnEnter(this, new CollisionInfo(){ other = other });
        }
            
    }

    private void Update()
    {
        if (doesMove)
            Move(World.Instance.currentSpeed + speedAdditive);
    }

    public void Move(float speed)
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }


    public void Fade(bool destroy = true)
    {
        float fadeDuration = 0.5f;

        spriteRenderer.DOFade(0f, fadeDuration).OnComplete(() =>
            {
                if (destroy)
                    Destroy(gameObject);
            });

    }
}
