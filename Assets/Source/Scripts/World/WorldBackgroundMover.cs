﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldBackgroundMover : MonoBehaviour
{
    public float dampening = 0.6f;
    public SpriteRenderer backgroundSprite;

    private void Start()
    {
        backgroundSprite = GetComponent < SpriteRenderer >();
    }

    public void Move(Vector2 velocity)
    {
        float speed = -(velocity.y * 0.5f) * dampening;
        backgroundSprite.material.mainTextureOffset += new Vector2(0f, speed);
    }
}
