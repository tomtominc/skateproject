﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using System.Linq;

public class SegmentMoveOptions
{
    public Zone zone;
    public Vector2 velocity;
    public SegmentType segmentType;
    public string segmentId = "NULL";
    public float speedOverride;
}

[RequireComponent(typeof(SegmentPool))]
public class WorldSegmentContainer : MonoBehaviour
{
    public WorldBackgroundMover background;

    private SegmentPool spawner;
    private WorldSegment lastSegment;
    private List < WorldSegment > activeSegments;

    private Vector3 bottom;
    private Vector3 top;

    private float orthographicSize
    {
        get { return Camera.main.orthographicSize; }
    }

    public WorldSegmentContainer Initialize()
    {
        spawner = GetComponent< SegmentPool >();
        bottom = new Vector3(0.5f, -orthographicSize - 0.6f);
        top = new Vector3(0.5f, orthographicSize + 0.6f);

        spawner.Collect();

        Refresh();

        return this;
    }


    public void Refresh()
    {
        spawner.DespawnAll();
        activeSegments = new List < WorldSegment >();
        lastSegment = null;

        WorldSegment initializerSegment = spawner.Single(SegmentType.Initializer);

        if (initializerSegment != null)
        {
            initializerSegment.Alloc();//.Place(Vector3.zero);//(new Vector3(0.5f, orthographicSize - 1f));
            initializerSegment.transform.localPosition = Vector3.zero;
            lastSegment = initializerSegment;
            lastSegment.Refresh();
            activeSegments.Add(lastSegment);
        }
    }

    public void Move(SegmentMoveOptions options)
    {
        UpdateSpawner(options);
        UpdateSegments(options.velocity);
        background.Move(options.velocity);
    }

    private void UpdateSpawner(SegmentMoveOptions options)
    {
        if (lastSegment == null || lastSegment.Bottom.y >= (bottom.y - 2f))
        {
            WorldSegment segment = spawner.Single(options);

            if (segment)
            {
                
                segment.Alloc();

                segment.Place(lastSegment == null ? bottom : lastSegment.Bottom);

                lastSegment = segment;
                lastSegment.Refresh();
                activeSegments.Add(lastSegment);
            }
        }
    }

    private void UpdateSegments(Vector2 velocity)
    {
        var removals = new List < WorldSegment >();

        foreach (WorldSegment segment in activeSegments)
        {
            segment.Translate(velocity);

            if (segment.Bottom.y >= top.y + 3f)
                removals.Add(segment);
        }

        foreach (var removal in removals)
        {
            activeSegments.Remove(removal);
            spawner.Despawn(removal);
        }
    }

}
