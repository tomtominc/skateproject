﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Framework;

public class SegmentPool : MonoBehaviour
{
    private List < WorldSegment > segments;
    public Dictionary < SegmentType , List < WorldSegment > > segmentPool;

    public void Collect()
    {
        segments = GetComponentsInChildren < WorldSegment >(true).ToList();
        segments.ForEach(x => x.Initialize());
        segmentPool = new Dictionary<SegmentType, List<WorldSegment>>();

        foreach (var segment in segments)
        {
            if (segmentPool.ContainsKey(segment.segmentType))
                segmentPool[segment.segmentType].Add(segment);
            else
                segmentPool.Add(segment.segmentType, new List < WorldSegment >() { segment });
        }
    }

    /// <summary>
    /// Returns a world segment of type SegmentType and at difficulty at random.
    /// WARNING: If the pool does not contain a segment of SegmentType
    /// it will return null.
    /// </summary>
    /// <param name="segmentType">Segment type.</param>
    public WorldSegment Single(SegmentType segmentType)
    {
        if (!segmentPool.ContainsKey(segmentType))
            return null;

        List < WorldSegment > currentSegments = segmentPool[segmentType]
            .Where(x => !x.gameObject.activeSelf).ToList();

        if (currentSegments.Count <= 0)
            return null;
        
        return new Selector().Single(currentSegments);
    }

    /// <summary>
    /// Returns a world segment of type SegmentType at random.
    /// WARNING: If the pool does not contain a segment of SegmentType
    /// it will return null.
    /// </summary>
    /// <param name="segmentType">Segment type.</param>
    public WorldSegment Single(SegmentMoveOptions options)
    {
        if (!segmentPool.ContainsKey(options.segmentType))
            return null;
        
        List < WorldSegment > currentSegments = 
            segmentPool[options.segmentType]
                .Where(x => !x.gameObject.activeSelf
                && x.id.Equals(options.segmentId)
                && x.zone == options.zone).ToList();

        if (currentSegments.Count <= 0)
            return null;

        return new Selector().Single(currentSegments);
    }

    public void Despawn(WorldSegment worldSegment)
    {
        worldSegment.gameObject.SetActive(false);
    }


    public void DespawnAll()
    {
        foreach (var segment in segments)
        {
            segment.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Get's the world segment 
    /// next in the list.
    /// Used only for Ordered Spawn Types
    /// </summary>
    public WorldSegment Next()
    {
        return null;
    }
    

}
