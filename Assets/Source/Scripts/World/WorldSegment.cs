﻿using System.Collections.Generic;
using CreativeSpore.SuperTilemapEditor;
using UnityEngine;
using System.Linq;
using Framework;

public enum Difficulty
{
    Easy,
    Medium,
    Hard,
    VeryHard
}

public enum SegmentType
{
    Initializer,
    Start,
    Main,
    End
}

public class WorldSegment : MonoBehaviour, ISelectable
{
    public Zone zone;
    public string id = "NULL";
    public SegmentType segmentType;
    public Difficulty difficulty;
    public Lane.Id bestLane;

    [Range(0f, 100f)]
    public float selectionWeight;

    private List < IBuildComponent > buildComponents;
    private TilemapGroup tileGroup;

    public bool ContainsBuildComponents
    {
        get { return buildComponents != null && buildComponents.Count > 0; }
    }

    public Vector3 Top
    {
        get
        { 
            return new Vector3(0f, transform.position.y) + Extends;
        }
    }

    public Vector3 Bottom
    {
        get
        {
            return new Vector3(0f, transform.position.y) - Extends;
        }
    }

    public Vector3 Extends
    {
        get { return new Vector3(0f, WorldBounds.extents.y); }
    }

    private BoxCollider2D boxCollider;

    [HideInInspector]
    public Bounds WorldBounds;

    private bool init;

    public virtual void Initialize()
    {
        buildComponents = GetComponentsInChildren < IBuildComponent >().ToList();

        tileGroup = GetComponent < TilemapGroup >();
        tileGroup.Refresh();
        tileGroup.Tilemaps.ForEach(x => WorldBounds.Encapsulate(x.MapBounds));

        init = true;
    }

    public virtual WorldSegment Alloc()
    {
        if (!init)
            Initialize();

        transform.localPosition = Vector3.zero;

        return this;
    }

    public virtual void Refresh()
    {
        gameObject.SetActive(true);


        if (ContainsBuildComponents)
            buildComponents.ForEach(x => x.Refresh());
    }

    public virtual void Place(Vector3 position)
    {
        transform.localPosition = position - Top;
    }

    public virtual void Translate(Vector2 velocity)
    {
        transform.Translate(velocity);
    }

    // ISelectable Interface
    public float GetSelectionWeight()
    {
        return selectionWeight;
    }

    public float AdjustedSelectionWeight { get; set; }

}
