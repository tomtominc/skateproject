﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public enum Direction
{
    Zero,
    Up,
    Down,
    Right,
    Left
}

public struct GameLayer
{
    public static LayerMask kNull { get { return LayerMask.GetMask("Null"); } }

    public static LayerMask Below { get { return LayerMask.GetMask("Below"); } }

    public static LayerMask Middle { get { return LayerMask.GetMask("Middle"); } }

    public static LayerMask Above { get { return LayerMask.GetMask("Above"); } }

    public static LayerMask Upper { get { return LayerMask.GetMask("Upper"); } }
}

public enum Zone
{
    Generic,
    Danger
}

public class World : Singleton < World >
{
    public const float pixelSize = 0.03125f;

    public Text metersLabel;
    public Text bestLabel;
    public Text coinsLabel;
    public int currentTheme = 0;
    public float startDelay = 1f;
    public float currentSpeed = 2f;

    [Range(0, 100)]
    public float difficulty = 0;

    [Header("World Zones")]
    public GenericZone genericZone;
    public DangerZone dangerZone;


    [HideInInspector]
    public float score = 0f;
    [HideInInspector]
    public Zone currentZone;

    private Dictionary < Zone , WorldZone > worldZones;

    private List < ObstacleSpawner > spawnersToRelease;

    public void Start()
    {
        spawnersToRelease = new List < ObstacleSpawner >();

        worldZones = new Dictionary < Zone, WorldZone >()
        {
            { Zone.Generic, genericZone },
            { Zone.Danger, dangerZone }
        };

        foreach (var zone in worldZones)
            zone.Value.Init(this);
    }

    public void InitializeWorld()
    {
        score = 0f;
        metersLabel.text = "Score\n00";
        bestLabel.text = string.Format("Best\n{0}", Bag.Get(ItemKey.Meters).BestValue.ToString("D2"));
        coinsLabel.text = "+00";
    }

    public void RefreshWorld()
    {
        foreach (var spawner in spawnersToRelease)
        {
            if (spawner != null)
                spawner.Release();
        }

        spawnersToRelease.Clear();

        if (worldZones.ContainsKey(currentZone))
            worldZones[currentZone].Refresh();
    }

    public void ChangeZone(Zone state)
    {
        if (worldZones.ContainsKey(currentZone))
            worldZones[currentZone].EndScene();

        currentZone = state;

        if (worldZones.ContainsKey(currentZone))
            worldZones[currentZone].StartScene();

    }

    public void CancelZone()
    {
        if (worldZones.ContainsKey(currentZone))
            worldZones[currentZone].CancelScene();
    }

    public void UpdateGameWorld()
    {
        if (worldZones.ContainsKey(currentZone))
            worldZones[currentZone].UpdateScene();
    }

    public void AddSpawnerForRelease(ObstacleSpawner spawner)
    {
        spawnersToRelease.Add(spawner);
    }

    public bool HasCompleteCondition()
    {
        if (worldZones.ContainsKey(currentZone))
            return worldZones[currentZone].HasCompleteCondition;

        return false;
    }

    public bool ZoneComplete()
    {
        if (worldZones.ContainsKey(currentZone))
            return worldZones[currentZone].IsComplete;

        return true;
    }

    public WorldZone GetWorldZone(Zone  zone)
    {
        if (worldZones.ContainsKey(zone))
            return worldZones[zone];

        return null;
    }

    public void UpdateMover(WorldSegmentContainer container, SegmentMoveOptions options)
    {
        float speed = options.speedOverride > 0 ? options.speedOverride : currentSpeed;
        options.velocity = Vector2.up * speed * Time.deltaTime;
        container.Move(options);
        UpdateMetersLabel(options.velocity);
    }

    public void UpdateMetersLabel(Vector2 velocity)
    {
        score += velocity.y / 4f;
        metersLabel.text = string.Format("Score\n{0}", ((int)score).ToString("D2"));
        coinsLabel.text = string.Format("+{0}", Bag.Get(ItemKey.Coins).SessionValue.ToString("D2"));
    }
  
}
