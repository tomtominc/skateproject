﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchObstacleSpawner : ObstacleSpawner
{
    public override void SpawnObstacles(bool leftLane, bool middleLane, bool rightLane)
    {
        ((HazardBolt)Spawn(Lane.Left)).SetActive(leftLane);
        ((HazardBolt)Spawn(Lane.Middle)).SetActive(middleLane);
        ((HazardBolt)Spawn(Lane.Right)).SetActive(rightLane);
    }
}
