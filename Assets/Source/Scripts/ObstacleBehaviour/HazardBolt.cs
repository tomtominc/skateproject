﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;
using DG.Tweening;

public class HazardBolt : ObstacleBehaviour
{
    public BoxCollider2D hazardCollider;
    public SpriteAnimation tileAnimator;
    public SpriteAnimation boltAnimator;
    public SpriteAnimation explosionAnimator;

    private Vector3 _target;
    private bool hasActivated = true;

    public void SetActive(bool active)
    {
        hasActivated = !active;

        if (active)
            tileAnimator.Play("Activate");
        else
            tileAnimator.Play("Normal");

        boltAnimator.Play("Normal");
        explosionAnimator.Play("Normal");
    }

    public override void SetTarget(Vector3 target)
    {
        _target = target;
    }

    private void Update()
    {
        if (hasActivated)
            return;
        
        float distance = Vector3.Distance(position, _target);

        if (distance < 0.1f)
        {
            hasActivated = true;
            Sequence sequence = DOTween.Sequence();

            sequence.AppendCallback(() => boltAnimator.Play("Activate"));
            sequence.AppendInterval(0.16f);
            sequence.AppendCallback(() =>
                {
                    hazardCollider.enabled = true;
                    explosionAnimator.Play("Explode");
                    GameManager.Instance.ShakeCamera();

                });

            sequence.AppendInterval(0.32f);
            sequence.AppendCallback(() => hazardCollider.enabled = false);

        }
    }



}
