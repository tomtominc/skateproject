﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Framework;

public class HazardBalloon : ObstacleBehaviour
{
    public float duration = 0.5f;
    public float inflateRadius = 0.1f;
    public SpriteAnimation animator;

    private bool hasInflated = false;

    public override void SetTarget(Vector3 target)
    {
        animator.Play("Normal");
        hasInflated = false;
    }

    private void Update()
    {
        float distance = Vector3.Distance(character.position, position);
        float x = Mathf.Abs(character.position.x - position.x);

        if (distance < inflateRadius && x < 1f && !hasInflated)
        {
            animator.Play("Inflate");
            hasInflated = true;
        }
    }
}
