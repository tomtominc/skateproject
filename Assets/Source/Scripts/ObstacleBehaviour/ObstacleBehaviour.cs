﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBehaviour : Entity
{
    protected Character character;

    private void Start()
    {
        character = Character.Instance;
    }

    private void OnEnable()
    {
        Enabled();
    }

    protected virtual void Enabled()
    {
        
    }

    public virtual void SetTarget(Vector3 target)
    {
        
    }
}
