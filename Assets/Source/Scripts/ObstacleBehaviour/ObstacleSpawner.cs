﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathologicalGames;
using System.Linq;

[RequireComponent(typeof(SpawnPool))]
public class ObstacleSpawner : MonoBehaviour
{
    public float targetYOffset = -0.5f;
    public float outsideYValue = 6f;
    public bool objectsMoveAfterCollision = true;

    public string spawnAlert = "Alert";
    public List < string > obstaclePrefabs;

    private SpawnPool pool;
    private List < WorldObject > obstacles;
    public System.EventHandler <WorldObject.CollisionInfo> Collision;

    private void Start()
    {
        obstacles = new List<WorldObject>();
        pool = GetComponent < SpawnPool >();
    }

    private void Update()
    {
        if (obstacles.Count <= 0)
            return;
        
        var removals = obstacles.Where(x => x.transform.position.y > outsideYValue).ToList();

        if (removals.Count <= 0)
            return;
        
        obstacles.RemoveAll(x => x.transform.position.y > outsideYValue);

        foreach (var obstacle in removals)
        {
            pool.Despawn(obstacle.transform);
        }

    }

    public virtual void SpawnObstacles(bool leftLane, bool middleLane, bool rightLane)
    {
        if (leftLane)
            StartCoroutine(Spawn(Lane.Left));
        if (middleLane)
            StartCoroutine(Spawn(Lane.Middle));
        if (rightLane)
            StartCoroutine(Spawn(Lane.Right));
    }

    protected virtual IEnumerator Spawn(Lane lane)
    {
        Transform alert = pool.Spawn(spawnAlert);
        alert.position = new Vector3(lane.position.x, -Camera.main.orthographicSize + 1f);
        SpriteAnimation alertAnimation = alert.GetComponent < SpriteAnimation >();

        alertAnimation.Play("Alert");

        yield return new WaitForSeconds(alertAnimation.GetCurrentAnimationLength());

        pool.Despawn(alert);

        Transform obstacle = pool.Spawn(obstaclePrefabs[Random.Range(0, obstaclePrefabs.Count)]);
        ObstacleBehaviour obstacleBehaviour = obstacle.GetComponent < ObstacleBehaviour >();
        WorldObject worldObject = obstacle.GetComponent < WorldObject >();

        obstacle.position = new Vector3(lane.position.x, -(Camera.main.orthographicSize + 1f));
        obstacleBehaviour.SetTarget(new Vector3(lane.position.x, lane.position.y + targetYOffset));
        worldObject.OnEnter += OnCollision; 

        obstacles.Add(worldObject);
    }

    public void OnCollision(object sender, WorldObject.CollisionInfo collisionInfo)
    {
        if (!objectsMoveAfterCollision)
        {
            foreach (var obstacle in obstacles)
            {
                obstacle.doesMove = false;
            }
        }

        if (Collision != null)
            Collision(sender, collisionInfo);
    }

    public void Release()
    {
        pool.DespawnAll();
    }

    public void DumpAssets()
    {
        World.Instance.AddSpawnerForRelease(this);
    }
}
