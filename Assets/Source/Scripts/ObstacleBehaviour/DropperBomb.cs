﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Com.LuisPedroFonseca.ProCamera2D;

public class DropperBomb : ObstacleBehaviour
{
    public float minScale = 0.5f;
    public float maxScale = 1f;

    public Ease dropEase = Ease.InQuad;
    public float dropDuration = 1.0f;
    public float bombInPercent = 0.75f;

    public Transform shadow;
    public Transform bomb;
    public Transform explosionPrefab;

    public override void SetTarget(Vector3 target)
    {
        bomb.gameObject.SetActive(false);
        bomb.localScale = new Vector3(2f, 2f, 2f);
        bomb.position = target;
        shadow.localScale = new Vector3(minScale, minScale, minScale);

        float bombDuration = dropDuration * bombInPercent;

        shadow.DOMove(target, dropDuration).SetEase(Ease.InQuad);
        shadow.DOScale(maxScale, dropDuration).SetEase(Ease.InQuad);
        bomb.DOScale(maxScale, dropDuration * (1f - bombInPercent)).SetEase(Ease.InQuad).OnStart(() => bomb.gameObject.SetActive(true)).SetDelay(bombDuration).OnComplete(() =>
            {
                Transform explosion = Instantiate < Transform >(explosionPrefab);
                explosion.position = target;
                ProCamera2DShake.Instance.Shake("GunShot");

                Destroy(gameObject);
            });
    }
}
