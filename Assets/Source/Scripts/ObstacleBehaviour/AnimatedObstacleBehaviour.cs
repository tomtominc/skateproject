﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public class AnimatedObstacleBehaviour : ObstacleBehaviour
{
    private SpriteAnimation spriteAnimation;

    protected override void Enabled()
    {
        spriteAnimation = GetComponent < SpriteAnimation >();
        spriteAnimation.Play("Active");
    }
}
