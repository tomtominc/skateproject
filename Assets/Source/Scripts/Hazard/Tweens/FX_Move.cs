﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Serialization;

public class FX_Move : FX
{
    public Vector3[] movePoints;
    public float duration;
    public PathType pathType;
    public PathMode pathMode;
    public int loops;
    public LoopType loopType;

    public override void DoEffect()
    {
        transform.DOLocalPath(movePoints, duration, pathType, pathMode, 10, Color.red).SetEase(Ease.Linear).SetLoops(loops, loopType);
    }

    public void OnDrawGizmos()
    {
        for (int i = 1; i < movePoints.Length; i++)
        {
            Vector3 pos = transform.localPosition;

            Gizmos.color = i % 2 == 0 ? Color.red : Color.green;
            Gizmos.DrawSphere(pos + movePoints[i - 1], 0.1f);
            Gizmos.DrawSphere(pos + movePoints[i], 0.1f);
            Gizmos.DrawLine(pos + movePoints[i - 1], pos + movePoints[i]);

        }
    }
}
