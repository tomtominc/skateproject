﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class FX  : MonoBehaviour
{
    public float startDelay = 0.1f;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(startDelay);
        DoEffect();
    }

    public abstract void DoEffect();

}
