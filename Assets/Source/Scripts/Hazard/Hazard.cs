﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Framework;

public enum CollisionBehaviour
{
    KnockBack,
    FlyForward,
    Bump
}

[RequireComponent(typeof(BoxCollider2D))]
public class Hazard : MonoBehaviour, IBuildComponent
{
    public string animationToPlay = "Normal";

    public CollisionBehaviour frontCollision;
    public CollisionBehaviour sideCollision;

    public AnimationStateInfo deathAnimation = AnimationStateInfo.Death;

    public LayerMask blockingLayers;

    public virtual LayerMask BlockingLayers
    {
        get { return blockingLayers; }
    }

    private SpriteRenderer _spriteRenderer;
    private SpriteAnimation _spriteAnimation;
    private BoxCollider2D _boxCollider2D;

    public virtual void Refresh()
    {
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _spriteAnimation = GetComponent < SpriteAnimation >();
        if (_spriteAnimation)
            _spriteAnimation.Play(animationToPlay);
    }

    public virtual void Flash()
    {

    }

    public virtual void HitMove(Vector2 direction)
    {
        _boxCollider2D.enabled = false;

    }

}
