﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public class BlockerSwitch : Hazard, ITriggerConnector
{
    public int id;
    public LayerMask blockAfterTrigger;

    public int Id
    { 
        get { return id; } 
    }

    protected bool triggered = false;
    protected SpriteAnimation spriteAnimation;
    protected SpriteRenderer spriteRenderer;

    public override LayerMask BlockingLayers
    {
        get{ return triggered ? blockAfterTrigger : blockingLayers; }
    }

    public override void Refresh()
    {
        if (spriteAnimation == null)
            spriteAnimation = GetComponent < SpriteAnimation >();

        if (spriteRenderer == null)
            spriteRenderer = GetComponent < SpriteRenderer >();

        spriteRenderer.sortingLayerName = "Middle";

        triggered = false;
        spriteAnimation.Play("Normal");

    }


    public void Trigger()
    {
        
        triggered = true;
        spriteRenderer.sortingLayerName = "Below";
        spriteAnimation.Play("Triggered");
    }

}
