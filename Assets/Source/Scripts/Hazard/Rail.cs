﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rail : Hazard, IPlatform
{
    public enum RailDirection
    {
        Straight,
        Right,
        Left
    }

    public LayerMask slideLayers;
    public RailDirection railDirection;
    private Lane toLane;

    private void Start()
    {
        Lane currentLane = transform.position.x < 0f ? Lane.Left : transform.position.x > 0f ? Lane.Right : Lane.Middle;

        if (railDirection == RailDirection.Right)
            toLane = currentLane.GetRight();

        if (railDirection == RailDirection.Left)
            toLane = currentLane.GetLeft();

        if (railDirection == RailDirection.Straight)
            toLane = currentLane;

    }

    public Lane ToLane
    {
        get { return toLane; }
    }

    public LayerMask Layers
    {
        get { return slideLayers; }
    }
}
