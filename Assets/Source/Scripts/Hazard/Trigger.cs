﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Trigger : MonoBehaviour, IBuildComponent
{
    public int id = 0;
    public ITriggerConnector connector;

    protected bool triggered = false;

    public void Refresh()
    {
        triggered = false;
        connector = transform.parent.GetComponentsInChildren < ITriggerConnector >(true).ToList().Find(x => x.Id == id);
    }

    public void DoTrigger()
    {
        if (triggered)
            return;

        triggered = true;

        if (connector != null)
        {
            connector.Trigger();
        }
    }
}
