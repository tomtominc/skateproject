﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITriggerConnector
{
    int Id { get; }

    void Trigger();
}
