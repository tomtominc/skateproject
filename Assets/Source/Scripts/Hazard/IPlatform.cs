﻿using UnityEngine;


public interface IPlatform
{
    LayerMask Layers { get; }

    Lane ToLane { get; }
}
