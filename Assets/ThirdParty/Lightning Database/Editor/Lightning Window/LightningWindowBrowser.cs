﻿using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using RutCreate;


namespace RutCreate.LightningDatabase
{
	public partial class LightningWindow
	{
		private const float ToggleColumnWidth = 20f;
		private const float StatusBarHeight = 34f;
		private const float ScrollbarSize = 15f;
		private const float TableHeaderHeight = 19f;
		private const float TableHeaderBorderWidth = 1f;
		private const float TableRowHeight = 20f;
		private const float TableRowSpacing = 2f;
		private const float TableColumnPadding = 1f;
		private const float TableFooterHeight = 25f;
		private const float TableMarginTop = 4f;

		private const float MinColumnWidth = 50f;
		private const float ResizeColumnHandleSize = 10f;

		private bool m_IsResizingColumn = false;
		private int m_ResizingColumnIndex = -1;
		private float m_ResizingStartWidth = 0f;
		private float m_ResizingStartMousePosition = 0f;
		private float m_TableWidth = 0f;

		private int[] m_ItemsPerPageValues = new int[] { 10, 20, 50, 100 };
		private string[] m_ItemsPerPageOptions;

		private List<int> m_SelectionIndices = new List<int>();
		private bool m_SelectAll = false;

		private List<Rect> m_Positions = new List<Rect>();
		private Rect m_DropFeedbackRect;
		private bool m_ShouldDisplayDropFeedback;

		private object m_ReferenceObject;

		private void DrawBrowserTable()
		{
			if (m_DataManager == null) return;

			m_DataManager.StartRecordUndo();
			DrawBrowserTableHeader();
			DrawBrowserTableData();
			DrawBrowserTableFooter();
			m_DataManager.StopRecordUndo();
		}

		private void DrawBrowserTableHeader()
		{
			Rect rect = new Rect();
			rect.x = InitialSidebarWidth;
			rect.width = position.width - InitialSidebarWidth;
			rect.y = StatusBarHeight + TableMarginTop;
			rect.height = TableHeaderHeight;

			// Reserve layout area.
			GUILayoutUtility.GetRect(rect.width, rect.height);

			GUI.BeginGroup(rect);

			Rect columnPosition = new Rect();
			columnPosition.x = -Mathf.Clamp(m_BrowserScrollPosition.x, 0f, float.MaxValue); // Move along the vertical scrollbar.
			columnPosition.y = TableHeaderBorderWidth;
			columnPosition.width = ToggleColumnWidth;
			columnPosition.height = rect.height;

			GUI.Box(columnPosition, "", "OL Title");

			Rect togglePosition = columnPosition;
			togglePosition.x += 3f;
			togglePosition.y += 1f;
			bool selectAll = GUI.Toggle(togglePosition, m_SelectAll, "");
			if (selectAll != m_SelectAll)
			{
				ClearSelection();

				if (selectAll == true)
				{
					for (int index = m_DataManager.StartItemIndex; index <= m_DataManager.EndItemIndex; index++)
					{
						m_SelectionIndices.Add(index);
					}
				}
				m_SelectAll = selectAll;
			}

			for (int i = 0; i < CurrentClass.Fields.Count; i++)
			{
				Field field = CurrentClass.Fields[i];
				columnPosition.x += columnPosition.width;
				columnPosition.width = field.ColumnWidth;

				GUI.Box(columnPosition, field.Name, RCGUIStyles.BrowserTableHeader);

				Rect handleRect = columnPosition;
				handleRect.width = ResizeColumnHandleSize;
				handleRect.x = (columnPosition.x + columnPosition.width) - handleRect.width * 0.5f;
				EditorGUIUtility.AddCursorRect(handleRect, MouseCursor.SplitResizeLeftRight);

				switch (Event.current.type)
				{
					case EventType.MouseDown:
						if (handleRect.Contains(Event.current.mousePosition))
						{
							m_IsResizingColumn = true;
							m_ResizingColumnIndex = i;
							m_ResizingStartWidth = field.ColumnWidth;
							m_ResizingStartMousePosition = Event.current.mousePosition.x + rect.x; // Plus offset from group.
							Event.current.Use();
						}
						break;
				}
			}
			GUI.EndGroup();

			m_TableWidth = columnPosition.xMax;

			if (m_IsResizingColumn && m_ResizingColumnIndex >= 0)
			{
				switch (Event.current.type)
				{
					case EventType.MouseDrag:
						Field field = CurrentClass.Fields[m_ResizingColumnIndex];
						float deltaX = Event.current.mousePosition.x - m_ResizingStartMousePosition;
						field.ColumnWidth = Mathf.Clamp(m_ResizingStartWidth + deltaX, MinColumnWidth, float.MaxValue);
						Repaint();
						Event.current.Use();
						break;
					case EventType.MouseUp:
						m_IsResizingColumn = false;
						Event.current.Use();
						break;
				}
			}
		}

		private void DrawBrowserTableData()
		{
			m_BrowserScrollPosition = EditorGUILayout.BeginScrollView(m_BrowserScrollPosition, GUILayout.Width(position.width - InitialSidebarWidth));
			int count = 0;
			for (int index = m_DataManager.StartItemIndex; index <= m_DataManager.EndItemIndex; index++)
			{
				if (m_DataManager.List.Count <= index) break;

				object item = m_DataManager.List[index];

				GUIStyle rowStyle = (index % 2 != 0) ? RCGUIStyles.BrowserTableRow : RCGUIStyles.BrowserTableRowStripe;
				EditorGUILayout.BeginHorizontal(rowStyle, GUILayout.MaxWidth(m_TableWidth));

				EditorGUILayout.BeginVertical(RCGUIStyles.BrowserTableCell, GUILayout.Width(ToggleColumnWidth));
				bool isSelected = m_SelectionIndices.Contains(index);
				bool selectedResult = EditorGUILayout.Toggle(isSelected);
				if (selectedResult != isSelected)
				{
					if (selectedResult == true)
					{
						m_SelectionIndices.Add(index);
						if (m_SelectionIndices.Count == (m_DataManager.EndItemIndex - m_DataManager.StartItemIndex + 1))
						{
							m_SelectAll = true;
						}
					}
					else
					{
						m_SelectAll = false;
						m_SelectionIndices.Remove(index);
					}
				}
				EditorGUILayout.EndVertical();

				for (int fieldIndex = 0; fieldIndex < CurrentClass.Fields.Count; fieldIndex++)
				{
					Field field = CurrentClass.Fields[fieldIndex];
					EditorGUILayout.BeginVertical(RCGUIStyles.BrowserTableCell, GUILayout.Width(field.ColumnWidth));
					DrawBrowserTableField(field, item);
					EditorGUILayout.EndVertical();
				}
				GUILayout.FlexibleSpace();
				EditorGUILayout.EndHorizontal();

				Rect rect = GUILayoutUtility.GetLastRect();
				rect.x += InitialSidebarWidth;
				rect.y += 35f + TableHeaderHeight + 3f - m_BrowserScrollPosition.y;
				if (m_Positions.Count <= count)
				{
					m_Positions.Add(rect);
				}
				else
				{
					m_Positions[count] = rect;
				}

				count++;
			}
			EditorGUILayout.EndScrollView();

			HandleEvents();
		}

		private void HandleEvents()
		{
			HandleContextMenuEvent();
			HandleDragEvent();
		}

		private void HandleContextMenuEvent()
		{
			int maxRows = Mathf.Clamp(m_Positions.Count, 0, m_DataManager.EndItemIndex - m_DataManager.StartItemIndex + 1);

			if (Event.current.type == EventType.ContextClick)
			{
				for (int i = 0; i < maxRows; i++)
				{
					Rect rowPosition = m_Positions[i];
					if (rowPosition.Contains(Event.current.mousePosition))
					{
						ShowTableRowContextMenu(m_DataManager.StartItemIndex + i);
						Event.current.Use();
						break;
					}
				}
			}
		}

		private void ShowTableRowContextMenu(int index)
		{
			GenericMenu menu = new GenericMenu();
			menu.AddItem(new GUIContent("Copy"), false, TableRowContextMenuCopy, index);
			if (m_ReferenceObject == null)
			{
				menu.AddDisabledItem(new GUIContent("Paste"));
			}
			else
			{
				menu.AddItem(new GUIContent("Paste"), false, TableRowContextMenuPaste, index);
			}
			menu.AddItem(new GUIContent("Duplicate"), false, TableRowContextMenuDuplicate, index);
			menu.AddItem(new GUIContent("Remove"), false, TableRowContextMenuRemove, index);
			menu.ShowAsContext();
		}

		private void TableRowContextMenuCopy(object param)
		{
			int index = (int)param;
			if (index >= 0 && m_DataManager.List.Count > index)
			{
				m_ReferenceObject = m_DataManager.List[index];
			}
		}

		private void TableRowContextMenuPaste(object param)
		{
			int index = (int)param;
			if (index >= 0 && m_DataManager.List.Count > index && m_ReferenceObject != null)
			{
				try
				{
					BaseClass oldItem = (BaseClass)m_DataManager.List[index];
					BaseClass newItem = (BaseClass)System.Activator.CreateInstance(m_DataManager.ModelType, new object[] { m_ReferenceObject });
					newItem.ID = oldItem.ID;
					m_DataManager.List[index] = newItem;
				}
				catch (MissingMethodException ex)
				{
					RebuildClass();
					Debug.LogWarning(ex.Message);
				}
			}
		}

		private void TableRowContextMenuDuplicate(object param)
		{
			int index = (int)param;
			if (index >= 0 && m_DataManager.List.Count > index)
			{
				try
				{
					BaseClass newItem = (BaseClass)System.Activator.CreateInstance(m_DataManager.ModelType, new object[] { m_DataManager.List[index] });

					m_DataManager.AddItem();
					BaseClass lastItem = (BaseClass)m_DataManager.List[m_DataManager.Count - 1];
					newItem.ID = lastItem.ID;
					m_DataManager.List[m_DataManager.Count - 1] = newItem;

					m_DataManager.ItemsPerPage = m_DataManager.ItemsPerPage;
					m_DataManager.LastPage();
				}
				catch (MissingMethodException ex)
				{
					RebuildClass();
					Debug.LogWarning(ex.Message);
				}
			}
		}

		private void TableRowContextMenuRemove(object param)
		{
			int index = (int)param;
			if (index >= 0 && m_DataManager.List.Count > index)
			{
				if (EditorUtility.DisplayDialog(
					    "Remove selected row",
					    "Are your sure you want to remove selected row?",
					    "Remove", "No, keep it"))
				{
					m_DataManager.List.RemoveAt(index);
				}
			}
		}

		private void RebuildClass()
		{
			Class klass = CurrentProject.Classes.Find(item => item.Name == m_DataManager.ModelType.Name);
			if (EditorUtility.DisplayDialog(
				    "Rebuild class",
				    "Copy and paste function does not work properly. This class need to be rebuild. We recommend you to backup all assets before do this.",
				    "Rebuild", "Not now"))
			{
				if (klass != null)
				{
					ClassBuilder.Rebuild(klass);
				}
				else
				{
					Debug.LogError("Something went wrong with copy & paste function. Please contact the author of this asset.");
				}
			}
		}

		private void HandleDragEvent()
		{
			int maxRows = Mathf.Clamp(m_Positions.Count, 0, m_DataManager.EndItemIndex - m_DataManager.StartItemIndex + 1);

			if (Event.current.type == EventType.MouseDrag)
			{
				for (int i = 0; i < maxRows; i++)
				{
					Rect rowPosition = m_Positions[i];
					if (rowPosition.Contains(Event.current.mousePosition))
					{
						DragAndDrop.PrepareStartDrag();
						DragAndDrop.SetGenericData("Object", m_DataManager.List[m_DataManager.StartItemIndex + i]);
						DragAndDrop.SetGenericData("Index", m_DataManager.StartItemIndex + i);
						DragAndDrop.StartDrag("Drag");
						Event.current.Use();
						m_ShouldDisplayDropFeedback = true;
						m_DropFeedbackRect = rowPosition;
						break;
					}
				}
			}
			else if (Event.current.type == EventType.DragUpdated)
			{
				DragAndDrop.visualMode = DragAndDropVisualMode.Move;

				for (int i = 0; i < maxRows; i++)
				{
					Rect rowPosition = m_Positions[i];

					if (i == 0)
					{
						if (Event.current.mousePosition.y <= rowPosition.y + rowPosition.height / 2f)
						{
							m_DropFeedbackRect = rowPosition;
							break;
						}
					}
					else
					{
						rowPosition = m_Positions[i];
						rowPosition.y -= m_Positions[i - 1].height / 2f;
						rowPosition.height = m_Positions[i - 1].height / 2f + rowPosition.height / 2f;
						if (rowPosition.Contains(Event.current.mousePosition))
						{
							m_DropFeedbackRect = m_Positions[i];
							break;
						}
					}

					if (i == maxRows - 1)
					{
						if (Event.current.mousePosition.y > rowPosition.y + rowPosition.height / 2f)
						{
							m_DropFeedbackRect = m_Positions[i];
							m_DropFeedbackRect.y += m_DropFeedbackRect.height;
							break;
						}
					}
				}

				Event.current.Use();
			}
			else if (Event.current.type == EventType.DragPerform)
			{
				int oldIndex = (int)DragAndDrop.GetGenericData("Index");
				int newIndex = oldIndex;

				for (int i = 0; i < maxRows; i++)
				{
					Rect rowPosition = m_Positions[i];

					if (i == 0)
					{
						if (Event.current.mousePosition.y <= rowPosition.y + rowPosition.height / 2f)
						{
							newIndex = m_DataManager.StartItemIndex + i;
							break;
						}
					}
					else
					{
						rowPosition = m_Positions[i];
						rowPosition.y -= m_Positions[i - 1].height / 2f;
						rowPosition.height = m_Positions[i - 1].height / 2f + rowPosition.height / 2f;
						if (rowPosition.Contains(Event.current.mousePosition))
						{
							newIndex = m_DataManager.StartItemIndex + i;
							if (newIndex > oldIndex)
							{
								newIndex -= 1;
							}
							break;
						}
					}

					if (i == maxRows - 1)
					{
						if (Event.current.mousePosition.y > rowPosition.y + rowPosition.height / 2f)
						{
							newIndex = m_DataManager.StartItemIndex + i + 1;
							break;
						}
					}
				}

				m_DataManager.List.Move(oldIndex, Mathf.Clamp(newIndex, 0, m_DataManager.EndItemIndex));
				m_ShouldDisplayDropFeedback = false;
				Event.current.Use();
			}

			if (m_ShouldDisplayDropFeedback)
			{
				m_DropFeedbackRect.height = 4f;
				GUI.Box(m_DropFeedbackRect, "", "PR Insertion Above");
			}
		}

		private void DrawBrowserTableField(Field field, object item)
		{
			FieldType fieldType = System.Activator.CreateInstance(field.Type) as FieldType;
			Type itemType = item.GetType();
			string fieldName = field.Name;
			if (fieldType.IsClassField)
			{
				fieldName += "ID";
			}

			PropertyInfo property = itemType.GetProperty(fieldName);
			if (property == null)
			{
				EditorGUILayout.LabelField("Error", GUILayout.MinWidth(0f));
				return;
			}
			object value = property.GetValue(item, null);

			if (Util.IsList(value))
			{
				IList list = value as IList;
				if (list.Count == 0)
				{
					EditorGUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("", RCGUIStyles.ButtonPlus))
					{
						list.Add(fieldType.GetDefaultValue());
					}
					GUILayout.FlexibleSpace();
					EditorGUILayout.EndHorizontal();
				}
				else
				{
					int indexToAdd = -1;
					int indexToRemove = -1;
					for (int i = 0; i < list.Count; i++)
					{
						EditorGUILayout.BeginHorizontal();
						list[i] = fieldType.DrawField(list[i]);

						if (GUILayout.Button("", RCGUIStyles.ButtonPlus))
						{
							indexToAdd = i;
						}

						if (GUILayout.Button("", RCGUIStyles.ButtonMinus))
						{
							indexToRemove = i;
						}
						EditorGUILayout.EndHorizontal();
					}

					if (indexToAdd >= 0)
					{
						list.Insert(indexToAdd + 1, fieldType.GetDefaultValue());
					}

					if (indexToRemove >= 0)
					{
						list.RemoveAt(indexToRemove);
					}
				}
			}
			else
			{
				if (field.Name == "ID")
				{
					EditorGUILayout.LabelField(value.ToString(), GUILayout.MinWidth(0f));
				}
				else
				{
					fieldType.DrawField(item as BaseClass, fieldName);
				}
			}
		}

		private void DrawBrowserTableFooter()
		{
			EditorGUILayout.BeginHorizontal(RCGUIStyles.BrowserTableFooter, GUILayout.MaxHeight(TableFooterHeight));

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Add new row", EditorStyles.miniButton))
			{
				m_DataManager.AddItem();
				m_DataManager.LastPage();
			}

			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();

			GUI.enabled = (m_SelectionIndices.Count > 0);
			if (GUILayout.Button("Remove selected", EditorStyles.miniButton))
			{
				if (EditorUtility.DisplayDialog(
					    "Remove selected row(s)",
					    string.Format("Are your sure you want to remove {0} selected row(s)?", m_SelectionIndices.Count),
					    "Remove", "No, keep it"))
				{
					for (int i = m_SelectionIndices.Count - 1; i >= 0; i--)
					{
						int index = m_SelectionIndices[i];
						if (index < m_DataManager.List.Count)
						{
							m_DataManager.List.RemoveAt(index);
						}
					}
					ClearSelection();

					// Need to recalculate total page in case of remove all items in that page.
					m_DataManager.ItemsPerPage = m_DataManager.ItemsPerPage;
				}
			}
			GUI.enabled = m_Editable;

			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			GUILayout.FlexibleSpace();

			if (m_ItemsPerPageOptions == null || m_ItemsPerPageOptions.Length != m_ItemsPerPageValues.Length)
			{
				m_ItemsPerPageOptions = new string[m_ItemsPerPageValues.Length];
				for (int i = 0; i < m_ItemsPerPageValues.Length; i++)
				{
					m_ItemsPerPageOptions[i] = m_ItemsPerPageValues[i].ToString();
				}
			}

			EditorGUI.BeginChangeCheck();

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			int itemsPerPage = EditorGUILayout.IntPopup(m_DataManager.ItemsPerPage, m_ItemsPerPageOptions, m_ItemsPerPageValues, GUILayout.Width(50f));
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			if (EditorGUI.EndChangeCheck())
			{
				m_DataManager.ItemsPerPage = itemsPerPage;
				EditorPrefs.SetInt(CurrentClass.Name + "_IPP", m_DataManager.ItemsPerPage);
				ClearSelection();
			}

			GUI.enabled = (m_DataManager.Page > 1);

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Prev", EditorStyles.miniButton))
			{
				m_DataManager.PreviousPage();
				ClearSelection();
			}
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			string pagingString = string.Format("{0}/{1}", m_DataManager.Page, m_DataManager.TotalPage);
			EditorGUILayout.LabelField(pagingString, GUILayout.Width(50f));
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			GUI.enabled = (m_DataManager.Page < m_DataManager.TotalPage);

			EditorGUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Next", EditorStyles.miniButton))
			{
				m_DataManager.NextPage();
				ClearSelection();
			}
			GUILayout.FlexibleSpace();
			EditorGUILayout.EndVertical();

			GUI.enabled = m_Editable;

			EditorGUILayout.EndHorizontal();
		}

		private void ClearSelection()
		{
			m_SelectionIndices.Clear();
			m_SelectAll = false;
		}
	}
}
