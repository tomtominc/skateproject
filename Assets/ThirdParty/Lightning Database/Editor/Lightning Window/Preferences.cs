﻿using UnityEngine;
using UnityEditor;


public class Preferences
{
	public const string AutoSaveIntervalPrefsKey = "AutoSaveInterval";
	public const float AutoSaveIntervalDefault = 5f;

	private static bool s_Loaded = false;
	private static float s_AutoSaveInterval;

	public static float AutoSaveInterval
	{
		get { return EditorPrefs.GetFloat(AutoSaveIntervalPrefsKey, AutoSaveIntervalDefault); }
	}

	[PreferenceItem("Lightning DB")]
	public static void LightningDatabasePreferencesGUI()
	{
		if (!s_Loaded)
		{
			Load();
			s_Loaded = true;
		}

		DrawGUI();

		if (GUI.changed)
		{
			Save();
		}
	}

	private static void DrawGUI()
	{
		s_AutoSaveInterval = EditorGUILayout.FloatField("Auto Save Interval", s_AutoSaveInterval);
	}

	private static void Load()
	{
		s_AutoSaveInterval = AutoSaveInterval;
	}

	private static void Save()
	{
		EditorPrefs.SetFloat(AutoSaveIntervalPrefsKey, s_AutoSaveInterval);
	}
}
