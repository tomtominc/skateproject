// Credit https://bitbucket.org/rotorz/classtypereference-for-unity

using UnityEngine;

using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Field : DotLiquid.Drop, ISerializationCallbackReceiver
	{
		[SerializeField]
		protected string m_Name;

		public string Name
		{
			get { return m_Name; }
			set { m_Name = value; }
		}

		private Type m_Type;

		public Type Type
		{
			get { return m_Type; }
			set
			{
				if (value != null && !value.IsClass)
				{
					throw new ArgumentException(string.Format("'{0}' is not a class type.", value.FullName), "value");
				}

				m_Type = value;
				m_ClassRef = GetClassRef(value);
			}
		}

		public static string GetClassRef(Type type)
		{
			return (type != null)
				? type.FullName + ", " + type.Assembly.GetName().Name
					: "";
		}

		[SerializeField]
		private string m_ClassRef;

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			if (!string.IsNullOrEmpty(m_ClassRef))
			{
				m_Type = System.Type.GetType(m_ClassRef);
				if (m_Type == null)
				{
					Debug.LogWarningFormat("'{0}' was referenced but class type was not found.", m_ClassRef);
				}
			}
			else
			{
				m_Type = null;
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		[SerializeField]
		protected bool m_IsList;

		public bool IsList
		{
			get { return m_IsList; }
			set { m_IsList = value; }
		}

		[SerializeField]
		protected bool m_IsClassField;

		public bool IsClassField
		{
			get { return m_IsClassField; }
			set { m_IsClassField = value; }
		}

		public string ClassFieldName
		{
			get
			{
				FieldType fieldType = (FieldType)System.Activator.CreateInstance(this.Type);
				return fieldType.ClassFieldName;
			}
		}

		public float ColumnWidth = 100f;

		public string FormattedType
		{
			get
			{ 
				string formattedType = EditorUtil.GetFieldTypeString(this.Type);
				if (m_IsList)
				{
					formattedType = string.Format("List<{0}>", formattedType);
				}

				return formattedType;
			}
		}

		public string OriginalType
		{
			get
			{
				string typeName = EditorUtil.GetFieldTypeType(this.Type).Name;
				return typeName;
			}
		}

		public string PlayMakerType
		{
			get
			{
				string playMakerType = string.Empty;
				if (this.Type == typeof(FloatField))
				{
					playMakerType = "FsmFloat";
				}
				else if (this.Type == typeof(IntField))
				{
					playMakerType = "FsmInt";
				}
				else if (this.Type == typeof(BooleanField))
				{
					playMakerType = "FsmBool";
				}
				else if (this.Type == typeof(GameObjectField))
				{
					playMakerType = "FsmGameObject";
				}
				else if (this.Type == typeof(StringField))
				{
					playMakerType = "FsmString";
				}
				else if (this.Type == typeof(Vector2Field))
				{
					playMakerType = "FsmVector2";
				}
				else if (this.Type == typeof(Vector3Field))
				{
					playMakerType = "FsmVector3";
				}
				else if (this.Type == typeof(ColorField))
				{
					playMakerType = "FsmColor";
				}
				else if (this.Type == typeof(RectField))
				{
					playMakerType = "FsmRect";
				}
				else if (this.Type == typeof(MaterialField))
				{
					playMakerType = "FsmMaterial";
				}
				else if (this.Type == typeof(TextureField))
				{
					playMakerType = "FsmTexture";
				}
				return playMakerType;
			}
		}

		public bool IsSerializable
		{
			get
			{
				return Attribute.GetCustomAttribute(this.Type, typeof(SerializableAttribute)) != null;
			}
		}

		public string DefaultValue
		{
			get
			{
				string defaultValue = string.Empty;

				if (m_IsList)
				{
					defaultValue = string.Format("new {0}()", FormattedType);
				}
				else
				{
					defaultValue = EditorUtil.GetFieldTypeDefaultValue(this.Type);
				}

				return defaultValue;
			}
		}

		public string CName
		{
			get
			{
				return m_Name.ToUppercaseFirst();
			}
		}

		public Field(string name, Type type, bool isList = false, bool isClassField = false)
		{
			// TODO
			// Validate type. It needs to derive from FieldType.
			m_Name = name;
			Type = type;
			m_IsList = isList;
			m_IsClassField = isClassField;
		}

		public Field(Field source)
		{
			m_Name = source.Name;
			Type = source.Type;
			m_IsList = source.IsList;
			m_IsClassField = source.IsClassField;
		}

		public override string ToString()
		{
			return m_Name;
		}
	}
}
