﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("AudioClip", "Unity", typeof(AudioClip), "AudioClip", "null")]
	public class AudioClipField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (AudioClip)item, typeof(AudioClip), false);
			return item;
		}
	}
}
