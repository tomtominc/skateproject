﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Texture2D", "Unity", typeof(Texture2D), "Texture2D", "null")]
	public class Texture2DField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (Texture2D)item, typeof(Texture2D), false, GUILayout.Width(64f), GUILayout.Height(64f));
			return item;
		}
	}
}
