﻿using UnityEngine;
using UnityEditor;
using System.Linq;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Rect", "Unity", typeof(Rect), "Rect", "new Rect()")]
	[System.Serializable]
	public class RectField : FieldType
	{
		public override object DrawField(object item)
		{
			Rect rect = (item == null) ? new Rect() : (Rect)item;
			item = EditorGUILayout.RectField(rect);
			return item;
		}

		public override object GetDefaultValue()
		{
			return new Rect();
		}
	}
}
