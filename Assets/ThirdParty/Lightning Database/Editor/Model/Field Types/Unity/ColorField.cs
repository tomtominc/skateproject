﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Color", "Unity", typeof(Color), "Color", "new Color(1f, 1f, 1f, 1f)")]
	[System.Serializable]
	public class ColorField : FieldType
	{
		public override object DrawField(object item)
		{
			Color color = (item == null) ? new Color(1f, 1f, 1f, 1f) : (Color)item;
			item = EditorGUILayout.ColorField(color);
			return item;
		}

		public override object GetDefaultValue()
		{
			return new Color(1f, 1f, 1f, 1f);
		}
	}
}
