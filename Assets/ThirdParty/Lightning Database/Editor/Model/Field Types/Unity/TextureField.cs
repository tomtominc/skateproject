﻿using UnityEngine;
using UnityEditor;

using System;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Texture", "Unity", typeof(Texture), "Texture", "null")]
	public class TextureField : FieldType
	{
		public override void DrawField(BaseClass item, string fieldName)
		{
			PropertyInfo propertyInfo = item.GetType().GetProperty(fieldName);
			if (propertyInfo != null)
			{
				object value = propertyInfo.GetValue(item, null);
				value = DrawField((value == null) ? null : (Texture)value);
				propertyInfo.SetValue(item, value, null);
			}
		}

		public override object DrawField(object item)
		{
			Texture texture = (item == null) ? null : (Texture)item;
			item = EditorGUILayout.ObjectField(texture, typeof(Texture), false, GUILayout.Width(64f), GUILayout.Height(64f));
			return item;
		}
	}
}
