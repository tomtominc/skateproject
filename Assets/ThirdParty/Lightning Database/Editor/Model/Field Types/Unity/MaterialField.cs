﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Material", "Unity", typeof(Material), "Material", "null")]
	public class MaterialField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (Material)item, typeof(Material), false);
			return item;
		}
	}
}
