﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Vector3", "Unity", typeof(Vector3), "Vector3", "Vector3.zero")]
	[System.Serializable]
	public class Vector3Field : FieldType
	{
		public override object DrawField(object item)
		{
			Vector3 vector = (item == null) ? Vector3.zero : (Vector3)item;
			item = EditorGUILayout.Vector3Field(GUIContent.none, vector, GUILayout.MinWidth(0f), GUILayout.Height(EditorGUIUtility.singleLineHeight));
			return item;
		}

		public override object GetDefaultValue()
		{
			return Vector3.zero;
		}
	}
}
