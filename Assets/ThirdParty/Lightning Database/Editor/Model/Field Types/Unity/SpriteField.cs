﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Sprite", "Unity", typeof(Sprite), "Sprite", "null")]
	public class SpriteField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (Sprite)item, typeof(Sprite), false, GUILayout.Width(64f), GUILayout.Height(64f));
			return item;
		}
	}
}
