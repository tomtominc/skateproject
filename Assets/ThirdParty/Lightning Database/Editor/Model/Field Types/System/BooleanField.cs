﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Boolean", "System", typeof(bool), "bool", "false")]
	[System.Serializable]
	public class BooleanField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.Toggle((item == null) ? false : (bool)item);
			return item;
		}

		public override object GetDefaultValue()
		{
			return false;
		}
	}
}
