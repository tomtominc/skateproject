﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;


namespace RutCreate
{
	public static class RCGUILayoutUtility
	{
		private static Dictionary<string, Texture2D> mTextures = new Dictionary<string, Texture2D>();

		public static Texture2D CreateBackgroundTextureColor(Color color)
		{
			#if UNITY_5_2 || UNITY_5_3_OR_NEWER
			string hexString = ColorUtility.ToHtmlStringRGBA(color);
			#else
			string hexString = color.ToHexStringRGBA();
			#endif
			if (!mTextures.ContainsKey(hexString) || mTextures[hexString] == null)
			{
				Texture2D texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
				texture.SetPixel(0, 0, color);
				texture.Apply();
				texture.hideFlags = HideFlags.HideAndDontSave;
				mTextures.Add(hexString, texture);
			}
			return mTextures[hexString];
		}

		public static Texture2D CreateBackgroundTextureColor(float r, float g, float b, float a = 1f)
		{
			Color color = new Color(r, g, b, a);
			return CreateBackgroundTextureColor(color);
		}

		public static Color HighlightedColor
		{
			get
			{
				Color color = new Color();
				color.r = EditorGUIUtility.isProSkin ? 0.24f : 0.23f;
				color.g = EditorGUIUtility.isProSkin ? 0.37f : 0.48f;
				color.b = EditorGUIUtility.isProSkin ? 0.60f : 0.92f;
				color.a = 1f;
				return color;
			}
		}

		public static Color BorderColor
		{
			get
			{
				Color color = new Color();
				color.r = EditorGUIUtility.isProSkin ? 0.15f : 0.53f;
				color.g = EditorGUIUtility.isProSkin ? 0.15f : 0.53f;
				color.b = EditorGUIUtility.isProSkin ? 0.15f : 0.53f;
				color.a = 1f;
				return color;
			}
		}

		public static Color GetTextColor()
		{
			var color = new Color();
			color.r = EditorGUIUtility.isProSkin ? 0.69f : 0f;
			color.g = EditorGUIUtility.isProSkin ? 0.69f : 0f;
			color.b = EditorGUIUtility.isProSkin ? 0.69f : 0f;
			color.a = 1f;
			return color;
		}

		public static float mLabelWidth;

		public static void BeginLabelWidth(float width)
		{
			mLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = width;
		}

		public static void EndLabelWidth()
		{
			EditorGUIUtility.labelWidth = mLabelWidth;
		}
	}
}
