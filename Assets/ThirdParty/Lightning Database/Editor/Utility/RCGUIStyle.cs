﻿using UnityEngine;
using UnityEditor;


namespace RutCreate
{
	// TODO
	// Cache styles.
	public static class RCGUIStyles
	{
		
		private static GUIStyle m_ClassItem;

		public static GUIStyle ClassItem
		{
			get
			{
				if (m_ClassItem == null)
				{
					m_ClassItem = new GUIStyle("Label");
				}
				Color backgroundColor = EditorGUIUtility.isProSkin ? new Color(0.24f, 0.37f, 0.6f) : new Color(0.23f, 0.48f, 0.92f);
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(backgroundColor);
				m_ClassItem.onNormal.background = background;
				m_ClassItem.onNormal.textColor = Color.white;
				m_ClassItem.active.background = background;
				m_ClassItem.active.textColor = Color.black;
				m_ClassItem.onActive.background = background;
				m_ClassItem.onActive.textColor = m_ClassItem.active.textColor;

				m_ClassItem.fixedHeight = 20;
				m_ClassItem.alignment = TextAnchor.MiddleLeft;
				m_ClassItem.stretchHeight = true;
				m_ClassItem.margin = new RectOffset(0, 0, 0, 0);
				m_ClassItem.padding = new RectOffset(10, 0, 0, 0);
				return m_ClassItem;
			}
		}

		private static GUIStyle m_Splitter;

		public static GUIStyle Splitter
		{
			get
			{
				if (m_Splitter == null)
				{
					m_Splitter = new GUIStyle();
				}
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(Color.black);
				m_Splitter.normal.background = background;
				m_Splitter.active.background = m_Splitter.normal.background;
				m_Splitter.focused.background = m_Splitter.normal.background;
				return m_Splitter;
			}
		}

		private static GUIStyle m_ProjectTopBarBg;

		public static GUIStyle ProjectTopBarBg
		{
			get
			{
				if (m_ProjectTopBarBg == null)
				{
					m_ProjectTopBarBg = new GUIStyle("ProjectBrowserTopBarBg");
				}
				m_ProjectTopBarBg.border = new RectOffset(1, 1, 1, 1);
				m_ProjectTopBarBg.alignment = TextAnchor.MiddleLeft;
				m_ProjectTopBarBg.fontSize = 14;
				m_ProjectTopBarBg.fixedHeight = 35;
				m_ProjectTopBarBg.stretchHeight = true;
				m_ProjectTopBarBg.wordWrap = true;
				m_ProjectTopBarBg.margin = new RectOffset(0, 0, 0, 4);
				m_ProjectTopBarBg.padding = new RectOffset(10, 10, 0, 0);
				return m_ProjectTopBarBg;
			}
		}

		private static GUIStyle m_ProjectTitle;

		public static GUIStyle ProjectTitle
		{
			get
			{
				if (m_ProjectTitle == null)
				{
					m_ProjectTitle = new GUIStyle("Label");
				}
				m_ProjectTitle.alignment = TextAnchor.MiddleLeft;
				m_ProjectTitle.fontSize = 14;
				m_ProjectTitle.fixedHeight = 35;
				m_ProjectTitle.stretchHeight = true;
				m_ProjectTitle.wordWrap = true;
				m_ProjectTitle.padding = new RectOffset(10, 0, 0, 0);
				return m_ProjectTitle;
			}
		}

		private static GUIStyle m_ClassesTopic;

		public static GUIStyle ClassesTopic
		{
			get
			{
				if (m_ClassesTopic == null)
				{
					m_ClassesTopic = new GUIStyle("Label");
				}
				m_ClassesTopic.border = new RectOffset(1, 1, 1, 1);
				m_ClassesTopic.alignment = TextAnchor.MiddleLeft;
				m_ClassesTopic.fontSize = 12;
				m_ClassesTopic.fontStyle = FontStyle.Bold;
				m_ClassesTopic.fixedHeight = 35;
				m_ClassesTopic.stretchHeight = true;
				m_ClassesTopic.wordWrap = true;
				m_ClassesTopic.margin = new RectOffset(0, 0, 0, 0);
				m_ClassesTopic.padding = new RectOffset(0, 0, 0, 0);
				return m_ClassesTopic;
			}
		}

		private static GUIStyle m_ContentBackground;

		public static GUIStyle ContentBackground
		{
			get
			{
				if (m_ContentBackground == null)
				{
					m_ContentBackground = new GUIStyle("Box");
				}
				float colorRGB = EditorGUIUtility.isProSkin ? 0.247f : 0.87f;
				Color color = new Color(colorRGB, colorRGB, colorRGB);
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(color);
				m_ContentBackground.normal.background = background;
				m_ContentBackground.active.background = m_ContentBackground.normal.background;
				m_ContentBackground.focused.background = m_ContentBackground.normal.background;
				m_ContentBackground.margin = new RectOffset(0, 0, 0, 0);
				m_ContentBackground.padding = new RectOffset(0, 0, 0, 0);
				return m_ContentBackground;
			}
		}

		private static GUIStyle m_BrowserStatusBar;

		public static GUIStyle BrowserStatusBar
		{
			get
			{
				if (m_BrowserStatusBar == null)
				{
					m_BrowserStatusBar = new GUIStyle(ProjectTitle);
				}
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(new Color(0.21f, 0.2f, 0.27f));
				m_BrowserStatusBar.normal.background = background;
				m_BrowserStatusBar.active.background = m_BrowserStatusBar.normal.background;
				m_BrowserStatusBar.focused.background = m_BrowserStatusBar.normal.background;
				m_BrowserStatusBar.normal.textColor = Color.white;
				m_BrowserStatusBar.active.textColor = m_BrowserStatusBar.normal.textColor;
				m_BrowserStatusBar.focused.textColor = m_BrowserStatusBar.normal.textColor;
				return m_BrowserStatusBar;
			}
		}

		private static GUIStyle m_BrowserStatusBarTitle;

		public static GUIStyle BrowserStatusBarTitle
		{
			get
			{
				if (m_BrowserStatusBarTitle == null)
				{
					m_BrowserStatusBarTitle = new GUIStyle("Label");
				}
				m_BrowserStatusBarTitle.fontStyle = FontStyle.Bold;
				m_BrowserStatusBarTitle.alignment = TextAnchor.MiddleLeft;
				m_BrowserStatusBarTitle.fontSize = 12;
				m_BrowserStatusBarTitle.fixedHeight = 35;
				m_BrowserStatusBarTitle.stretchHeight = true;
				m_BrowserStatusBarTitle.wordWrap = true;
				m_BrowserStatusBarTitle.margin = new RectOffset(0, 0, 0, 0);
				m_BrowserStatusBarTitle.padding = new RectOffset(0, 0, 0, 0);
				return m_BrowserStatusBarTitle;
			}
		}

		private static GUIStyle m_BrowserStatusBarSubtitle;

		public static GUIStyle BrowserStatusBarSubtitle
		{
			get
			{
				if (m_BrowserStatusBarSubtitle == null)
				{
					m_BrowserStatusBarSubtitle = new GUIStyle(BrowserStatusBarTitle);
				}
				m_BrowserStatusBarSubtitle.fontStyle = FontStyle.Normal;
				m_BrowserStatusBarSubtitle.fontSize = 10;
				m_BrowserStatusBarSubtitle.fixedHeight = 35;
				m_BrowserStatusBarSubtitle.stretchHeight = true;
				m_BrowserStatusBarSubtitle.wordWrap = true;
				m_BrowserStatusBarSubtitle.margin = new RectOffset(0, 0, 0, 0);
				m_BrowserStatusBarSubtitle.padding = new RectOffset(10, 0, 3, 0);
				return m_BrowserStatusBarSubtitle;
			}
		}

		private static GUIStyle m_BrowserStatusBarButton;

		public static GUIStyle BrowserStatusBarButton
		{
			get
			{
				if (m_BrowserStatusBarButton == null)
				{
					m_BrowserStatusBarButton = new GUIStyle("Button");
				}
				m_BrowserStatusBarButton.fixedHeight = 20;
				m_BrowserStatusBarButton.margin.top = 7;
				return m_BrowserStatusBarButton;
			}
		}

		private static GUIStyle m_BrowserTableHeaderBackground;

		public static GUIStyle BrowserTableHeaderBackground
		{
			get
			{
				if (m_BrowserTableHeaderBackground == null)
				{
					m_BrowserTableHeaderBackground = new GUIStyle();
				}
				m_BrowserTableHeaderBackground.fixedHeight = 18;
				m_BrowserTableHeaderBackground.wordWrap = false;
				m_BrowserTableHeaderBackground.padding = new RectOffset(0, 0, 0, 0);
				m_BrowserTableHeaderBackground.margin = new RectOffset(0, 0, 0, 0);
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(new Color(0.3f, 0.3f, 0.3f));
				m_BrowserTableHeaderBackground.normal.background = background;
				m_BrowserTableHeaderBackground.active.background = m_BrowserTableHeaderBackground.normal.background;
				m_BrowserTableHeaderBackground.focused.background = m_BrowserTableHeaderBackground.normal.background;
				return m_BrowserTableHeaderBackground;
			}
		}

		private static GUIStyle m_BrowserTableHeader;

		public static GUIStyle BrowserTableHeader
		{
			get
			{
				if (m_BrowserTableHeader == null)
				{
					m_BrowserTableHeader = new GUIStyle("OL Title");
				}
				return m_BrowserTableHeader;
			}
		}

		private static GUIStyle m_BrowserTableDataBackground;

		public static GUIStyle BrowserTableDataBackground
		{
			get
			{
				if (m_BrowserTableDataBackground == null)
				{
					m_BrowserTableDataBackground = new GUIStyle();
				}
				m_BrowserTableDataBackground.fixedHeight = 0;
				m_BrowserTableDataBackground.wordWrap = true;
				m_BrowserTableDataBackground.padding = new RectOffset(0, 0, 0, 0);
				m_BrowserTableDataBackground.margin = new RectOffset(0, 0, 0, 0);
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(Color.white);
				m_BrowserTableDataBackground.normal.background = background;
				m_BrowserTableDataBackground.active.background = m_BrowserTableDataBackground.normal.background;
				m_BrowserTableDataBackground.focused.background = m_BrowserTableDataBackground.normal.background;
				return m_BrowserTableDataBackground;
			}
		}

		private static GUIStyle m_BrowserTableRow;

		public static GUIStyle BrowserTableRow
		{
			get
			{
				if (m_BrowserTableRow == null)
				{
					m_BrowserTableRow = new GUIStyle();
				}
				m_BrowserTableRow.fixedHeight = 0;
				m_BrowserTableRow.wordWrap = true;
				m_BrowserTableRow.padding = new RectOffset(0, 0, 0, 0);
				m_BrowserTableRow.margin = new RectOffset(0, 0, 0, 0);
				float colorRGB = EditorGUIUtility.isProSkin ? 0.235f : 0.87f;
				Color backgroundColor = new Color(colorRGB, colorRGB, colorRGB);
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(backgroundColor);
				m_BrowserTableRow.normal.background = background;
				m_BrowserTableRow.active.background = m_BrowserTableRow.normal.background;
				m_BrowserTableRow.focused.background = m_BrowserTableRow.normal.background;
				return m_BrowserTableRow;
			}
		}

		private static GUIStyle m_BrowserTableRowStripe;

		public static GUIStyle BrowserTableRowStripe
		{
			get
			{
				if (m_BrowserTableRowStripe == null)
				{
					m_BrowserTableRowStripe = new GUIStyle(BrowserTableRow);
				}
				float colorRGB = EditorGUIUtility.isProSkin ? 0.215f : 0.847f;
				Color backgroundColor = new Color(colorRGB, colorRGB, colorRGB);
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(backgroundColor);
				m_BrowserTableRowStripe.normal.background = background;
				m_BrowserTableRowStripe.active.background = m_BrowserTableRowStripe.normal.background;
				m_BrowserTableRowStripe.focused.background = m_BrowserTableRowStripe.normal.background;
				return m_BrowserTableRowStripe;
			}
		}

		private static GUIStyle m_BrowserTableCell;

		public static GUIStyle BrowserTableCell
		{
			get
			{
				if (m_BrowserTableCell == null)
				{
					m_BrowserTableCell = new GUIStyle(BrowserTableRow);
				}
				Texture2D background = RCGUILayoutUtility.CreateBackgroundTextureColor(Color.clear);
				m_BrowserTableCell.normal.background = background;
				m_BrowserTableCell.active.background = m_BrowserTableCell.normal.background;
				m_BrowserTableCell.focused.background = m_BrowserTableCell.normal.background;
				return m_BrowserTableCell;
			}
		}

		private static GUIStyle m_BrowserTableFooter;

		public static GUIStyle BrowserTableFooter
		{
			get
			{
				if (m_BrowserTableFooter == null)
				{
					m_BrowserTableFooter = new GUIStyle("ProjectBrowserBottomBarBg");
				}
				return m_BrowserTableFooter;
			}
		}

		private static GUIStyle m_BrowserTableFooterPager;

		public static GUIStyle BrowserTableFooterPager
		{
			get
			{
				if (m_BrowserTableFooterPager == null)
				{
					m_BrowserTableFooterPager = new GUIStyle("Label");
				}
				m_BrowserTableFooterPager.normal.textColor = Color.white;
				m_BrowserTableFooterPager.active.textColor = m_BrowserTableFooterPager.normal.textColor;
				m_BrowserTableFooterPager.focused.textColor = m_BrowserTableFooterPager.normal.textColor;
				m_BrowserTableFooterPager.alignment = TextAnchor.MiddleCenter;
				m_BrowserTableFooterPager.padding = new RectOffset(10, 10, 0, 0);
				m_BrowserTableFooterPager.stretchHeight = true;
				return m_BrowserTableFooterPager;
			}
		}

		private static GUIStyle m_ButtonPlus;

		public static GUIStyle ButtonPlus
		{
			get
			{
				if (m_ButtonPlus == null)
				{
					m_ButtonPlus = new GUIStyle("OL Plus");
				}
				m_ButtonPlus.margin.top = 2;
				m_ButtonPlus.fixedWidth = 16f;
				m_ButtonPlus.fixedHeight = 16f;
				return m_ButtonPlus;
			}
		}

		private static GUIStyle m_ButtonMinus;

		public static GUIStyle ButtonMinus
		{
			get
			{
				if (m_ButtonMinus == null)
				{
					m_ButtonMinus = new GUIStyle("OL Minus");
				}
				m_ButtonMinus.margin.top = 2;
				m_ButtonMinus.fixedWidth = 16f;
				m_ButtonMinus.fixedHeight = 16f;
				return m_ButtonMinus;
			}
		}
	}
}
