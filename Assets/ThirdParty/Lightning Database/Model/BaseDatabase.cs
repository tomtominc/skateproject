﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Text;


namespace RutCreate.LightningDatabase
{
    public abstract class BaseDatabase<T> : ScriptableObject where T : BaseClass
    {

        [SerializeField]
        protected List<T> m_Items = new List<T>();

        [SerializeField]
        [HideInInspector]
        protected int m_LastID = 0;

        public virtual void Add(T item)
        {
            item.ID = ++m_LastID;
            m_Items.Add(item);
        }

        public virtual void Remove(T item)
        {
            m_Items.Remove(item);
        }

        public virtual void Remove(int id)
        {
            m_Items.RemoveAll(item => item.ID == id);
        }

        public virtual void Remove(Predicate<T> match)
        {
            m_Items.RemoveAll(match);
        }

        public virtual void Clear()
        {
            m_Items.Clear();
        }

        public virtual void Reset()
        {
            Clear();
            m_LastID = 0;
        }

        public virtual T Find(int id)
        {
            return m_Items.Find(item => item.ID == id);
        }

        public virtual T Find(string name)
        {
            return m_Items.Find(item => item.Name == name);
        }

        public virtual T Find(Predicate<T> match)
        {
            return m_Items.Find(match);
        }

        public virtual List<T> FindAll()
        {
            return m_Items;
        }

        public virtual List<T> FindAll(int id)
        {
            return m_Items.FindAll(item => item.ID == id);
        }

        public virtual List<T> FindAll(string name)
        {
            return m_Items.FindAll(item => item.Name == name);
        }

        public virtual List<T> FindAll(Predicate<T> match)
        {
            return m_Items.FindAll(match);
        }

        public virtual int Count(Predicate<T> match = null)
        {
            return (match == null) ? m_Items.Count : FindAll(match).Count;
        }

        public virtual void Save(int slot = 1)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(m_LastID.ToString());

            foreach (T item in m_Items)
            {
                sb.AppendLine(item.GetSerializedData());
            }

            string filepath = Application.persistentDataPath + "/" + GetFilename(slot);
            File.Delete(filepath);
            StreamWriter sw = File.CreateText(filepath);

            if (sw == null)
            {
                Debug.LogErrorFormat("[{0}] save failed because cannot open file for writing at {1}.", this.GetType().Name, filepath);
                return;
            }

            sw.Write(sb.ToString());
            sw.Close();
        }

        public virtual void Load(int slot = 1)
        {
            string filepath = Application.persistentDataPath + "/" + GetFilename(slot);
            if (File.Exists(filepath))
            {
                m_Items.Clear();
                StreamReader sr = new StreamReader(filepath);
                string line = string.Empty;
                int count = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    if (count == 0)
                    {
                        m_LastID = RuntimeSerializer.ConvertToInt32(line);
                        count++;
                    }
                    else
                    {
                        CreateItemFromSerializedData(line);
                    }
                }
            }
            else
            {
                Save(slot);
            }
        }

        public virtual void CreateItemFromSerializedData(string serializedData)
        {
			
        }

        protected virtual string GetFilename(int slot = 1)
        {
            string filename = typeof(T).Name.ToLower() + "." + slot + ".sav";
            return filename;
        }
    }
}
