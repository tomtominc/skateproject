﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class BaseClass
	{
		protected Dictionary<string, string> m_SerializedFields;

		public BaseClass()
		{
			
		}

		public BaseClass(BaseClass source)
		{
			m_ID = source.ID;
			m_Name = source.Name;
		}

		public BaseClass(string serializedData)
		{
			m_SerializedFields = new Dictionary<string, string>();
			string[] columns = serializedData.Split(new string[] { RuntimeSerializer.FieldSeparator }, System.StringSplitOptions.None);
			foreach (string column in columns)
			{
				string[] keyValue = column.Split(new string[] { RuntimeSerializer.KeyValueSeparator }, System.StringSplitOptions.None);
				if (keyValue.Length != 2)
				{
					Debug.LogWarningFormat("Invalid format for serialized data on {0}: {1}", GetType().Name, column);
					return;
				}
				m_SerializedFields.Add(keyValue[0], keyValue[1]);
			}

			ID = RuntimeSerializer.ConvertToInt32(m_SerializedFields["ID"]);
			Name = m_SerializedFields["Name"];
		}

		#region Fields

		[SerializeField]
		protected int m_ID;

		public virtual int ID
		{
			get { return m_ID; }
			set { m_ID = value; }
		}

		[SerializeField]
		protected string m_Name;

		public virtual string Name
		{
			get { return m_Name; }
			set { m_Name = value; }
		}

		#endregion

		public void SetField(string field, object value)
		{
			PropertyInfo property = GetType().GetProperty(field);
			if (property != null)
			{
				property.SetValue(this, value, null);
			}
		}

		public void SetFieldAtIndex(int index, string field, object value)
		{
			PropertyInfo property = GetType().GetProperty(field);
			if (property != null)
			{
				IList list = property.GetValue(this, null) as IList;
				if (list != null && list.Count > index && index >= 0)
				{
					list[index] = value;
				}
			}
		}

		public virtual string GetSerializedData()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("ID");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(ID);
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Name");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(Name);
			return sb.ToString();
		}

		public override string ToString()
		{
			return string.Format("[{0}]: ID={1}, Name={2}", GetType().Name, ID, Name);
		}
	}

}
