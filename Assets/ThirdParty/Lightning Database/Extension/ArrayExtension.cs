﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace RutCreate
{
	public static class ArrayExtension
	{
		public static void Move(this IList list, int oldIndex, int newIndex)
		{
			if (list.Count == 0) return;
			if (oldIndex == newIndex) return;
			if (oldIndex >= list.Count) return;

			if (newIndex < 0)
			{
				newIndex = 0;
			}
			else if (newIndex >= list.Count)
			{
				newIndex = list.Count - 1;
			}

			object item = list[oldIndex];
			if (newIndex < oldIndex)
			{
				list.Insert(newIndex, item);
				list.RemoveAt(oldIndex + 1);
			}
			else
			{
				list.RemoveAt(oldIndex);
				list.Insert(newIndex, item);
			}
		}
	}
}
