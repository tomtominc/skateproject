﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace RutCreate.LightningDatabase
{
	public static class RuntimeSerializer
	{
		public const string DefaultListSeparator = ";";
		public const string ListSeparatorKey = "ListSeparator";

		public const string DefaultMultiValueSeparator = ":";
		public const string MultiValueSeparatorKey = "MultiValueSeparator";

		public const string DefaultKeyValueSeparator = "=";
		public const string KeyValueSeparatorKey = "KeyValueSeparator";

		public const string DefaultFieldSeparator = ",";
		public const string FieldSeparatorKey = "FieldSeparator";

		public static string ListSeparator
		{
			get { return PlayerPrefs.GetString(ListSeparatorKey, DefaultListSeparator); }
		}

		public static string MultiValueSeparator
		{
			get { return PlayerPrefs.GetString(MultiValueSeparatorKey, DefaultMultiValueSeparator); }
		}

		public static string KeyValueSeparator
		{
			get { return PlayerPrefs.GetString(KeyValueSeparatorKey, DefaultKeyValueSeparator); }
		}

		public static string FieldSeparator
		{
			get { return PlayerPrefs.GetString(FieldSeparatorKey, DefaultFieldSeparator); }
		}

		#region Conversion methods

		public static string ConvertToString(string stringValue)
		{
			return stringValue;
		}

		public static List<string> ConvertToListString(string stringValue)
		{
			List<string> values = new List<string>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToString(rawValues[i]));
			}
			return values;
		}

		public static int ConvertToInt32(string stringValue)
		{
			int value = 0;
			int.TryParse(stringValue, out value);
			return value;
		}

		public static List<int> ConvertToListInt32(string stringValue)
		{
			List<int> values = new List<int>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToInt32(rawValues[i]));
			}
			return values;
		}

		public static long ConvertToInt64(string stringValue)
		{
			long value = 0;
			long.TryParse(stringValue, out value);
			return value;
		}

		public static List<long> ConvertToListInt64(string stringValue)
		{
			List<long> values = new List<long>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToInt64(rawValues[i]));
			}
			return values;
		}

		public static float ConvertToSingle(string stringValue)
		{
			float value = 0;
			float.TryParse(stringValue, out value);
			return value;
		}

		public static List<float> ConvertToListSingle(string stringValue)
		{
			List<float> values = new List<float>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToSingle(rawValues[i]));
			}
			return values;
		}

		public static double ConvertToDouble(string stringValue)
		{
			double value = 0;
			double.TryParse(stringValue, out value);
			return value;
		}

		public static List<double> ConvertToListDouble(string stringValue)
		{
			List<double> values = new List<double>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToDouble(rawValues[i]));
			}
			return values;
		}

		public static bool ConvertToBoolean(string stringValue)
		{
			bool value = false;
			bool.TryParse(stringValue, out value);
			return value;
		}

		public static List<bool> ConvertToListBoolean(string stringValue)
		{
			List<bool> values = new List<bool>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToBoolean(rawValues[i]));
			}
			return values;
		}

		public static Vector2 ConvertToVector2(string stringValue)
		{
			string[] values = stringValue.Split(new string[] { MultiValueSeparator }, System.StringSplitOptions.None);
			if (values.Length != 2)
			{
				Debug.LogWarningFormat("Invalid format for Vector2 data: {0}", stringValue);
				return Vector2.zero;
			}
			Vector2 value = new Vector2(ConvertToSingle(values[0]), ConvertToSingle(values[1]));
			return value;
		}

		public static List<Vector2> ConvertToListVector2(string stringValue)
		{
			List<Vector2> values = new List<Vector2>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToVector2(rawValues[i]));
			}
			return values;
		}

		public static Vector3 ConvertToVector3(string rawValue)
		{
			string[] values = rawValue.Split(new string[] { MultiValueSeparator }, System.StringSplitOptions.None);
			if (values.Length != 3)
			{
				Debug.LogWarningFormat("Invalid format for Vector3 data: {0}", rawValue);
				return Vector3.zero;
			}
			Vector3 value = new Vector3(ConvertToSingle(values[0]),
				                ConvertToSingle(values[1]),
				                ConvertToSingle(values[2]));
			return value;
		}

		public static List<Vector3> ConvertToListVector3(string stringValue)
		{
			List<Vector3> values = new List<Vector3>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToVector3(rawValues[i]));
			}
			return values;
		}

		public static Vector4 ConvertToVector4(string rawValue)
		{
			string[] values = rawValue.Split(new string[] { MultiValueSeparator }, System.StringSplitOptions.None);
			if (values.Length != 4)
			{
				Debug.LogWarningFormat("Invalid format for Vector4 data: {0}", rawValue);
				return Vector4.zero;
			}
			Vector3 value = new Vector4(ConvertToSingle(values[0]),
				                ConvertToSingle(values[1]),
				                ConvertToSingle(values[2]),
				                ConvertToSingle(values[3]));
			return value;
		}

		public static List<Vector4> ConvertToListVector4(string stringValue)
		{
			List<Vector4> values = new List<Vector4>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToVector4(rawValues[i]));
			}
			return values;
		}

		public static Rect ConvertToRect(string rawValue)
		{
			string[] values = rawValue.Split(new string[] { MultiValueSeparator }, System.StringSplitOptions.None);
			if (values.Length != 4)
			{
				Debug.LogWarningFormat("Invalid format for Rect data: {0}", rawValue);
				return Rect.MinMaxRect(0, 0, 0, 0);
			}
			Rect value = new Rect(ConvertToSingle(values[0]),
				             ConvertToSingle(values[1]),
				             ConvertToSingle(values[2]),
				             ConvertToSingle(values[3]));
			return value;
		}

		public static List<Rect> ConvertToListRect(string stringValue)
		{
			List<Rect> values = new List<Rect>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToRect(rawValues[i]));
			}
			return values;
		}

		public static Color ConvertToColor(string rawValue)
		{
			string[] values = rawValue.Split(new string[] { MultiValueSeparator }, System.StringSplitOptions.None);
			if (values.Length != 4)
			{
				Debug.LogWarningFormat("Invalid format for Color data: {0}", rawValue);
				return Color.white;
			}
			Color value = new Color(ConvertToSingle(values[0]),
				              ConvertToSingle(values[1]),
				              ConvertToSingle(values[2]),
				              ConvertToSingle(values[3]));
			return value;
		}

		public static List<Color> ConvertToListColor(string stringValue)
		{
			List<Color> values = new List<Color>();
			string[] rawValues = stringValue.Split(new string[] { ListSeparator }, System.StringSplitOptions.None);
			for (int i = 0; i < rawValues.Length; i++)
			{
				values.Add(ConvertToColor(rawValues[i]));
			}
			return values;
		}

		#endregion

		#region Serialize methods

		public static string SerializeString(string value)
		{
			return value;
		}

		public static string SerializeString(List<string> values)
		{
			return string.Join(MultiValueSeparator, values.ToArray());
		}

		public static string SerializeInt32(int value)
		{
			return value.ToString();
		}

		public static string SerializeInt32(List<int> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeInt32(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeInt64(long value)
		{
			return value.ToString();
		}

		public static string SerializeInt64(List<long> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeInt64(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeSingle(float value)
		{
			return value.ToString();
		}

		public static string SerializeSingle(List<float> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeSingle(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeDouble(double value)
		{
			return value.ToString();
		}

		public static string SerializeDouble(List<double> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeDouble(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeBoolean(bool value)
		{
			return value.ToString();
		}

		public static string SerializeBoolean(List<bool> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeBoolean(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeColor(Color value)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(value.r);
			sb.Append(MultiValueSeparator);
			sb.Append(value.g);
			sb.Append(MultiValueSeparator);
			sb.Append(value.b);
			sb.Append(MultiValueSeparator);
			sb.Append(value.a);
			return sb.ToString();
		}

		public static string SerializeColor(List<Color> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeColor(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeVector2(Vector2 value)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(value.x);
			sb.Append(MultiValueSeparator);
			sb.Append(value.y);
			return sb.ToString();
		}

		public static string SerializeVector2(List<Vector2> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeVector2(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeVector3(Vector3 value)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(value.x);
			sb.Append(MultiValueSeparator);
			sb.Append(value.y);
			sb.Append(MultiValueSeparator);
			sb.Append(value.z);
			return sb.ToString();
		}

		public static string SerializeVector3(List<Vector3> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeVector3(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeVector4(Vector4 value)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(value.x);
			sb.Append(MultiValueSeparator);
			sb.Append(value.y);
			sb.Append(MultiValueSeparator);
			sb.Append(value.z);
			sb.Append(MultiValueSeparator);
			sb.Append(value.w);
			return sb.ToString();
		}

		public static string SerializeVector4(List<Vector4> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeVector4(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		public static string SerializeRect(Rect value)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(value.x);
			sb.Append(MultiValueSeparator);
			sb.Append(value.y);
			sb.Append(MultiValueSeparator);
			sb.Append(value.width);
			sb.Append(MultiValueSeparator);
			sb.Append(value.height);
			return sb.ToString();
		}

		public static string SerializeRect(List<Rect> values)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < values.Count; i++)
			{
				sb.Append(SerializeRect(values[i]));
				if (i < values.Count - 1)
				{
					sb.Append(ListSeparator);
				}
			}
			return sb.ToString();
		}

		#endregion
	}
}
