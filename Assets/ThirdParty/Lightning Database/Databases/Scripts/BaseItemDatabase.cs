using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseItemDatabase : BaseDatabase<Item>
	{
		

		protected virtual void OnEnable()
		{
			
		}

		public override void CreateItemFromSerializedData(string serializedData)
		{
			Item item = new Item(serializedData);
			m_Items.Add(item);
		}
	}
}
