using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Currency : BaseCurrency
	{
		public Currency() {}

		public Currency(Currency source) : base(source) {}

		public Currency(string serializedData) : base(serializedData) {}
	}
}
