using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Item : BaseItem
	{
		public Item() {}

		public Item(Item source) : base(source) {}

		public Item(string serializedData) : base(serializedData) {}
	}
}
