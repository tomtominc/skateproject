using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseItem : BaseClass
	{
		public BaseItem() {}

		public BaseItem(BaseItem source) : base(source)
		{
			
			
			m_Value = source.Value;
			m_LifetimeValue = source.LifetimeValue;
			m_SessionValue = source.SessionValue;
			m_BestValue = source.BestValue;
			m_Icon = source.Icon;
			m_Pickup = source.Pickup;
			m_Data = source.Data;
			
		}

		public BaseItem(string serializedData) : base(serializedData)
		{
			if (m_SerializedFields.ContainsKey("Value"))
			{
				Value = RuntimeSerializer.ConvertToInt32(m_SerializedFields["Value"]);
			}
			if (m_SerializedFields.ContainsKey("LifetimeValue"))
			{
				LifetimeValue = RuntimeSerializer.ConvertToInt32(m_SerializedFields["LifetimeValue"]);
			}
			if (m_SerializedFields.ContainsKey("SessionValue"))
			{
				SessionValue = RuntimeSerializer.ConvertToInt32(m_SerializedFields["SessionValue"]);
			}
			if (m_SerializedFields.ContainsKey("BestValue"))
			{
				BestValue = RuntimeSerializer.ConvertToInt32(m_SerializedFields["BestValue"]);
			}
			if (m_SerializedFields.ContainsKey("Data"))
			{
				Data = RuntimeSerializer.ConvertToString(m_SerializedFields["Data"]);
			}
			
		}

		[SerializeField]
		protected int m_Value = 0;

		public virtual int Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}
		[SerializeField]
		protected int m_LifetimeValue = 0;

		public virtual int LifetimeValue
		{
			get { return m_LifetimeValue; }
			set { m_LifetimeValue = value; }
		}
		[SerializeField]
		protected int m_SessionValue = 0;

		public virtual int SessionValue
		{
			get { return m_SessionValue; }
			set { m_SessionValue = value; }
		}
		[SerializeField]
		protected int m_BestValue = 0;

		public virtual int BestValue
		{
			get { return m_BestValue; }
			set { m_BestValue = value; }
		}
		[SerializeField]
		protected Sprite m_Icon = null;

		public virtual Sprite Icon
		{
			get { return m_Icon; }
			set { m_Icon = value; }
		}
		[SerializeField]
		protected Sprite m_Pickup = null;

		public virtual Sprite Pickup
		{
			get { return m_Pickup; }
			set { m_Pickup = value; }
		}
		[SerializeField]
		protected string m_Data = string.Empty;

		public virtual string Data
		{
			get { return m_Data; }
			set { m_Data = value; }
		}
		

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Value");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(Value));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("LifetimeValue");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(LifetimeValue));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("SessionValue");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(SessionValue));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("BestValue");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(BestValue));
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Data");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeString(Data));
			
			return sb.ToString();
		}
	}
}
