using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseCurrencyDatabase : BaseDatabase<Currency>
	{
		

		protected virtual void OnEnable()
		{
			
		}

		public override void CreateItemFromSerializedData(string serializedData)
		{
			Currency item = new Currency(serializedData);
			m_Items.Add(item);
		}
	}
}
