using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseCurrency : BaseClass
	{
		public BaseCurrency() {}

		public BaseCurrency(BaseCurrency source) : base(source)
		{
			
			
			m_Value = source.Value;
			
		}

		public BaseCurrency(string serializedData) : base(serializedData)
		{
			if (m_SerializedFields.ContainsKey("Value"))
			{
				Value = RuntimeSerializer.ConvertToInt32(m_SerializedFields["Value"]);
			}
			
		}

		[SerializeField]
		protected int m_Value = 0;

		public virtual int Value
		{
			get { return m_Value; }
			set { m_Value = value; }
		}
		

		public override string GetSerializedData()
		{
			string data = base.GetSerializedData();

			System.Text.StringBuilder sb = new System.Text.StringBuilder();
			sb.Append(data);
			
			sb.Append(RuntimeSerializer.FieldSeparator);
			sb.Append("Value");
			sb.Append(RuntimeSerializer.KeyValueSeparator);
			sb.Append(RuntimeSerializer.SerializeInt32(Value));
			
			return sb.ToString();
		}
	}
}
